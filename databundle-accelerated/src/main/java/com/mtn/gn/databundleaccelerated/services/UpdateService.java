package com.mtn.gn.databundleaccelerated.services;

public interface UpdateService {
    boolean updateRedis();
    boolean updateMongo();
    boolean updatePostgres();
    boolean updateOracle();
}
