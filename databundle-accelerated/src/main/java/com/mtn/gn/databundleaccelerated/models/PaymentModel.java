package com.mtn.gn.databundleaccelerated.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentModel{
    private String thirdPartyAcctRef;
    private String processingNumber;
    private String senderID;
    private String statusCode;
    private String statusDesc;
    private String momTransactionID;
    private SubscriberInfoModel subscriberInfo;
}
