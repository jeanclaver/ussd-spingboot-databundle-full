package com.mtn.gn.databundleaccelerated.collections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubMenuCollection implements Serializable {
    @JsonIgnore
    private String comment = null;
    private String _id;
    private int position;
    private String name;
    private int price;
    private String keyword;
    private String momoKeyword;
    private String si;
    private String momoSi;
    private List<SubMenuCollection> sub2eMenu;
}
