package com.mtn.gn.databundleaccelerated.proxies;

import com.mtn.gn.databundleaccelerated.config.MyConfigProperties;
import com.mtn.gn.databundleaccelerated.models.DatabundleTransactionModel;
import com.mtn.gn.databundleaccelerated.models.MdpAttachBundleResponse;
import com.mtn.gn.databundleaccelerated.models.MenuPrincipalModel;
import com.mtn.gn.databundleaccelerated.models.PaymentModel;
import kong.unirest.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnirestServiceProxy {
    @Autowired
    MyConfigProperties myConfigProperties;

    public PaymentModel sendRequestToEcw(String msisdn, int amount, String keyword) {
        boolean isLive = false;
        String sp = "momo100.sp";

        if (this.myConfigProperties.getEcwGatewayEnv().equals("LIVE")) {
         isLive = true;
        }

        if (!this.myConfigProperties.getEcwGatewayIp().trim().endsWith("24")) {
            sp = "momo100c.sp";
        }

        String url = "http://"+ this.myConfigProperties.getEcwGatewayIp() + ":" + this.myConfigProperties.getEcwGatewayPort() +"/ecw/api/v2/payment/"+ isLive +"/bundle/" +
                msisdn +
                "/" + amount +
                "/"+ sp +"/" +
                keyword +
                "/DATABUNDLE?message=DATABUNDLE_APP&narration=Forfait Internet";

        return Unirest.get(url)
                .basicAuth("mtn-momo", "Qwpatp2019@##*.")
                .header("Content-Type", "application/json")
                .socketTimeout(10000)
                .connectTimeout(10000)
                .asObject(PaymentModel.class)
                .getBody();
    }

    public MdpAttachBundleResponse sendRequestToMDP(String msisdn, String keyword) {
        String url = "http://"+ this.myConfigProperties.getMdpGatewayIp() + ":" + this.myConfigProperties.getMdpGatewayPort() + "/pcrf-api/attachBundle/" +
                keyword +
                "/" + msisdn +
                "/" + msisdn +
                "/1";

        return Unirest.get(url)
                .basicAuth("mtn-pcrf", "Qwerty2019@***.")
                .header("Content-Type", "application/json")
                .socketTimeout(20000)
                .connectTimeout(20000)
                .asObject(MdpAttachBundleResponse.class)
                .getBody();
    }

    public DatabundleTransactionModel sendTransaction (DatabundleTransactionModel data) {
        String url = this.myConfigProperties.getOracleInterfacer() + "/transactions/databundle";

        return Unirest.post(url)
                .header("Content-Type", "application/json")
                .body(data)
                .socketTimeout(20000)
                .connectTimeout(20000)
                .asObject(DatabundleTransactionModel.class)
                .getBody();
    }

    public MenuPrincipalModel saveToOracleDB (MenuPrincipalModel data) {
        String url = this.myConfigProperties.getOracleInterfacer() + "/menu/databundle/save";

        return Unirest.post(url)
                .header("Content-Type", "application/json")
                .body(data)
                .socketTimeout(10000)
                .connectTimeout(10000)
                .asObject(MenuPrincipalModel.class)
                .getBody();
    }
}
