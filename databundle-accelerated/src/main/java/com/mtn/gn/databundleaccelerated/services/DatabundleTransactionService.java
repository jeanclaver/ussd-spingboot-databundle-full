package com.mtn.gn.databundleaccelerated.services;


import com.mtn.gn.databundleaccelerated.collections.DatabundleTransactionCollection;

public interface DatabundleTransactionService {
    DatabundleTransactionCollection saveTransaction(DatabundleTransactionCollection databundleTransactionEntity);
}
