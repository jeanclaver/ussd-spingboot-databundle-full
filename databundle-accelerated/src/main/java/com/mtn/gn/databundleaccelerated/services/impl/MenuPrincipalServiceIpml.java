package com.mtn.gn.databundleaccelerated.services.impl;

import com.mtn.gn.databundleaccelerated.collections.MenuPrincipaleCollection;
import com.mtn.gn.databundleaccelerated.entities.MenuPrincipalEntity;
import com.mtn.gn.databundleaccelerated.repositories.MenuPrincipalFromPostgresRepository;
import com.mtn.gn.databundleaccelerated.repositories.MenuPrincipaleRepository;
import com.mtn.gn.databundleaccelerated.services.MenuPrincipaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuPrincipalServiceIpml implements MenuPrincipaleService {
    @Autowired
    private MenuPrincipaleRepository principal;

    @Autowired
    private MenuPrincipalFromPostgresRepository repositoryOracle;


    @Override
    public Iterable<MenuPrincipaleCollection> getMenu() {
        return this.principal.findAllByOrderByPosition();
    }

    @Override
    public MenuPrincipalEntity getMenuFromOracle() {
        return this.repositoryOracle.findByName("MENU");
    }

    @Override
    public MenuPrincipalEntity saveMenuToOracle(MenuPrincipalEntity menuPrincipalEntity) {
        return this.repositoryOracle.save(menuPrincipalEntity);
    }
}
