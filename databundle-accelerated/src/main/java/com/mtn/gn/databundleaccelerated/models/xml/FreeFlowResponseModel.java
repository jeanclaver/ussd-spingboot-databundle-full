package com.mtn.gn.databundleaccelerated.models.xml;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FreeFlowResponseModel {
    private String freeflowState = "FD";
    private String freeflowCharging = "N";
    private String freeflowChargingAmount = "0.0";
    private parameters parameters = new parameters(
            new ArrayList<>()
    );
}
