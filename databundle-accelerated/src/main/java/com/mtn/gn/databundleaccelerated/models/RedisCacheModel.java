package com.mtn.gn.databundleaccelerated.models;

import com.mtn.gn.databundleaccelerated.collections.DirectShortCodeCollection;
import com.mtn.gn.databundleaccelerated.collections.SubMenuCollection;
import com.mtn.gn.databundleaccelerated.enums.MenuEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RedisCacheModel implements Serializable {
    public int step;
    public String ussdSession;
    public int choice;
    public MenuEnum menu;
    public String msisdn;
    public String msg;
    public String driven;
    public String freeFlow;
    List<MenuItemCollection> menuItems = new ArrayList<>();
    SubMenuCollection confirmationMenu;
    DirectShortCodeCollection directSc;
}
