package com.mtn.gn.databundleaccelerated.repositories;

import com.mtn.gn.databundleaccelerated.collections.MenuPrincipaleCollection;
import org.springframework.data.repository.CrudRepository;

public interface MenuPrincipaleRepository extends CrudRepository<MenuPrincipaleCollection, String> {
    Iterable<MenuPrincipaleCollection> findAllByOrderByPosition();
}
