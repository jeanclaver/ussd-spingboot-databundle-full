package com.mtn.gn.databundleaccelerated.services.impl;

import com.mtn.gn.databundleaccelerated.collections.DatabundleTransactionCollection;
import com.mtn.gn.databundleaccelerated.repositories.DatabundleTransactionRepository;
import com.mtn.gn.databundleaccelerated.services.DatabundleTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DatabundleTransactionServiceImpl implements DatabundleTransactionService {
    @Autowired
    private DatabundleTransactionRepository databundleTransactionRepository;
    @Override
    public DatabundleTransactionCollection saveTransaction(DatabundleTransactionCollection databundleTransactionEntity) {
        return this.databundleTransactionRepository.save(databundleTransactionEntity);
    }
}
