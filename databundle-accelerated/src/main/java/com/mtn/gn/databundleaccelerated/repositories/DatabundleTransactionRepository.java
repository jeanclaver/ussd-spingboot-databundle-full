package com.mtn.gn.databundleaccelerated.repositories;

import com.mtn.gn.databundleaccelerated.collections.DatabundleTransactionCollection;
import org.springframework.data.repository.CrudRepository;

public interface DatabundleTransactionRepository extends CrudRepository<DatabundleTransactionCollection, String> {
}
