package com.mtn.gn.databundleaccelerated.services.impl;

import com.mtn.gn.databundleaccelerated.collections.DatabundleTransactionCollection;
import com.mtn.gn.databundleaccelerated.collections.DirectShortCodeCollection;
import com.mtn.gn.databundleaccelerated.collections.SubMenuCollection;
import com.mtn.gn.databundleaccelerated.entities.MenuPrincipalEntity;
import com.mtn.gn.databundleaccelerated.models.*;
import com.mtn.gn.databundleaccelerated.models.xml.request;
import com.mtn.gn.databundleaccelerated.proxies.UnirestServiceProxy;
import com.mtn.gn.databundleaccelerated.repositories.RedisCacheRepository;
import com.mtn.gn.databundleaccelerated.utils.MdpErrorCode;
import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.services.LogServiceImpl;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@Service
@Data
public class HelperService {
    private static final String MSG_SUCCESS = "Cher client, votre demande d achat internet est en cours de traitement.\n";
    private static final String MSG_YELLOTV = "MTN Yello TV! Plus 500 films & Series, + 20 chaines de Tele EN DIRECT sur votre smartphone et SANS CONSOMMER VOTRE PASS! Telechargez sur bit.ly/39Xwnu6 .\n";
    private static final String MSG_SUCCESS_MDP = "Votre requete est en cours de traitement. Vous recevrez un message dans un instant\n";
    private static final String MSG_ERREUR = "Une erreur est survenue, veillez reessayer plus tard! MTN vous remercie pour votre fidelite";
    private static final String NOT_ENOUGH_MONEY = "Cher client, vous n avez pas suffisamment d argent dans votre compte. Merci de recharger et reessayer.";

    @Autowired
    private MenuPrincipalServiceIpml menuP;

    @Autowired
    private RedisCacheRepository cacheRepo;

    @Autowired
    TransactionsServiceImpl oracleTransaction;

    @Autowired
    UnirestServiceProxy unirestServiceProxy;

    @Autowired
    private DatabundleTransactionServiceImpl mongoTransaction;

    public void saveCache (RedisCacheModel cache){
        this.cacheRepo.addItem(cache);
    }

    public RedisCacheModel getCache (String id){
        return this.cacheRepo.getItem(id);
    }

    public void deleteCache (String id){
        this.cacheRepo.deleteItem(id);
    }

    public String sendRequestToMoMo(int amount, String keyword, String msisdn) {
        PaymentModel resp;
        try {
            resp = this.unirestServiceProxy.sendRequestToEcw(
                    msisdn,
                    amount,
                    keyword
            );
            LogServiceImpl.Log(
                    this,
                    LogLevel.INFO,
                    (msisdn + "|****** RESPONSE FROM ECW WITH KEYWORD : "+ keyword +"  and price "+ amount + " GNF ******\n" + resp.toString()).toUpperCase());

            // println("******RESPONSE from ECW \n$resp********")
            if (resp.getStatusCode().equals("1000")) {
                return MSG_SUCCESS ;
            }
            else if (resp.getStatusCode().equals("529")) {
                return NOT_ENOUGH_MONEY;
            }

        } catch (Exception e) {
            LogServiceImpl.Log(
                    this,
                    LogLevel.ERROR,
                    (msisdn + "|****** ERROR SENDING REQUEST TO ECW  ******\n" + e.getMessage()).toUpperCase());
            e.printStackTrace();
        }

        return  MSG_ERREUR;
    }

    public String sendRequestToMdp(String keyword, String msisdn, int amount) {
        try {
            MdpAttachBundleResponse response = this.unirestServiceProxy.sendRequestToMDP(msisdn, keyword);

            LogServiceImpl.Log(
                    this,
                    LogLevel.INFO,
                    (msisdn + "|****** RESPONSE FROM MDP WITH KEYWORD : "+ keyword + " ******\n" + response.toString()).toUpperCase());


            if (response.getStatusCode() == 0) {
                // Saving transaction to mongo database
                this.mongoTransaction.saveTransaction(
                        new DatabundleTransactionCollection(
                                UUID.randomUUID().toString(),
                                msisdn,
                                "DATABUNDLE_AIRTIME",
                                amount,
                                keyword,
                                "SUCCESS",
                                new Date().toString()
                        )
                );

                boolean savedToOracle =  this.oracleTransaction.saveDatabundleTransaction(
                        new DatabundleTransactionModel(
                                UUID.randomUUID().toString(),
                                msisdn,
                                "DATABUNDLE_AIRTIME",
                                amount,
                                keyword,
                                "SUCCESS",
                                new Date().toString()
                        )
                );

                if (savedToOracle) {
                    LogServiceImpl.Log(
                            this,
                            LogLevel.INFO,
                            (msisdn + "|****** Transaction successfully save to oracle DB : "+ keyword +"  and price "+ amount + " GNF ******\n" + response.toString()).toUpperCase());
                } else {
                    LogServiceImpl.Log(
                            this,
                            LogLevel.ERROR,
                            (msisdn + "|****** Transaction failed save to oracle DB : " +
                                    ""+ keyword +"  and price "+ amount + " GNF ******\n" + response.toString()).toUpperCase());
                }

                return MSG_SUCCESS_MDP;
            }
            else {
                LogServiceImpl.Log(
                        this,
                        LogLevel.INFO,
                        (msisdn + "|****** MDP RESPONSE  ******\n" + response.toString()).toUpperCase());
                MdpErrorCode mdpErrorCode =  new MdpErrorCode();
                mdpErrorCode.handleErrorCode(response.getStatusCode());

                return mdpErrorCode.getErrorMsg();
            }
        } catch (Exception e) {
            LogServiceImpl.Log(
                    this,
                    LogLevel.ERROR,
                    (msisdn + "|****** ERROR SENDING REQUEST TO MDP  ******\n" + e.getMessage()).toUpperCase());

            e.printStackTrace();

            return MSG_ERREUR;
        }
    }

    /**
     * Sending payment request for others bundles except dayli
     * @param rq
     * @param bundle
     */
    public String sendPayment(@RequestBody request rq, SubMenuCollection bundle, RedisCacheModel cache) {
        StringBuilder menu = new StringBuilder();

        switch (rq.getSubscriberInput().trim()) {
            case "1": {
                LogServiceImpl.Log(
                        this,
                        LogLevel.INFO,
                        (rq.getMsisdn() + "|****** SENDING REQUEST TO MDP WITH KEYWORD: "+ bundle.getKeyword() +" ******").toUpperCase());
                if (bundle.getPosition() != 0) {
                    menu.append(
                            this.sendRequestToMdp(
                                    bundle.getKeyword(),
                                    rq.getMsisdn(),
                                    bundle.getPrice()
                            )
                    );
                } else {
                    menu.append(MSG_ERREUR);
                }
            }
            break;
            case "2": {
                LogServiceImpl.Log(
                        this,
                        LogLevel.INFO,
                        (rq.getMsisdn() + "|****** SENDING REQUEST TO ECW WITH KEYWORD: "+ bundle.getMomoKeyword() +"  and price "+ bundle.getPrice() +" GNF ******").toUpperCase());
                if (bundle.getPosition() != 0) {
                    menu.append(this.sendRequestToMoMo(
                            bundle.getPrice(),
                            bundle.getMomoKeyword(),
                            rq.getMsisdn()
                    ));
                } else {
                    menu.append(MSG_ERREUR);
                }
            }
            break;
        }
        cache.freeFlow = "FB";
        return menu.toString();
    }

    public String getPrincipalMenuFromRedis() {
        return  this.cacheRepo.getMenuPrincipal();
    }

    public MenuPrincipalEntity getPrincipalMenuFromPostgres() {
        return  this.menuP.getMenuFromOracle();
    }

    public String getConfirmationString(SubMenuCollection confirmationMenu, String validity, String addOnMoMo) {
        StringBuilder menu = new StringBuilder();
        menu.append("Confirmez-vous l achat du pass internet de ")
                .append(confirmationMenu.getName()).append(validity)
                .append("?\n");

        menu.append("Acheter\n");
        menu.append("1. Par credit\n");
        menu.append("2. Par MoMo").append(addOnMoMo).append("\n");
        menu.append("# Retour\n");

        return menu.toString();
    }

    public String getConfirmation24(MenuItemCollection confirmationMenu, String addOn) {
        StringBuilder menu = new StringBuilder();

        menu.append("Confirmez-vous l achat du pass internet de ")
                .append(confirmationMenu.getName())
                .append("?\n");

        menu.append("Acheter\n");
        menu.append("1. Par credit\n");
        menu.append("2. Par MoMo"+ addOn +"\n");
        menu.append("# Retour\n");

        return menu.toString();
    }

    public String directShortHandler(DirectShortCodeCollection sc) {
        MenuItemCollection menuItemCollection = new MenuItemCollection();
        menuItemCollection.setName(sc.getName());
        return this.getConfirmation24(
                menuItemCollection,
                null
        );
    }
}

