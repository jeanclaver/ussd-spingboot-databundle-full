package com.mtn.gn.databundleaccelerated.services;

import com.mtn.gn.databundleaccelerated.collections.DirectShortCodeCollection;

import java.util.List;

public interface DirectShortCodeService {
    DirectShortCodeCollection getShortCodeKeywords(int price);
    List<DirectShortCodeCollection> getShortCodes();
}
