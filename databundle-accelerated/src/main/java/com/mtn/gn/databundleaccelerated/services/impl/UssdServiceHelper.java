package com.mtn.gn.databundleaccelerated.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mtn.gn.databundleaccelerated.collections.MenuPrincipaleCollection;
import com.mtn.gn.databundleaccelerated.collections.SubMenuCollection;
import com.mtn.gn.databundleaccelerated.enums.MenuEnum;
import com.mtn.gn.databundleaccelerated.models.MenuItemCollection;
import com.mtn.gn.databundleaccelerated.models.RedisCacheModel;
import com.mtn.gn.databundleaccelerated.models.xml.request;
import com.mtn.gn.databundleaccelerated.repositories.RedisCacheRepository;
import com.mtn.gn.databundleaccelerated.utils.Minify;
import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.services.LogServiceImpl;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Data
@Service
public class UssdServiceHelper {
    private static final String DRIVEN_MOMO_ACCELERATE_CODE = "771";
    private static final String DRIVEN_YELLOWYA_TV_CODE = "773";
    private static final String DRIVEN_MON_COMPTE_CODE = "774";
    private static final String MSG_ERREUR = "Une erreur est survenue, veillez reessayer plus tard! MTN vous remercie pour votre fidelite";

    List<MenuPrincipaleCollection> menuPrincipale = new ArrayList<>();

    @Autowired
    private MenuPrincipalServiceIpml menuPrincipalServiceIpml;

    @Autowired
    HelperService helper;

    @Autowired
    RedisCacheRepository redisCacheRepository;


    public String getPrincipaleMenu(request rq, RedisCacheModel cache) {
        StringBuilder menu = new StringBuilder();
        principalMenuHandler(rq, menu);

        cache.freeFlow = "FC";
        cache.driven = null;
        return menu.toString();
    }

    private void principalMenuHandler(request rq, StringBuilder menu) {
        try {
            // GETTING PRINCIPAL FROM REDIS CACHE
            String redisMenu = this.helper.getPrincipalMenuFromRedis();
            // WHEN REDIS IS EMPTY CALL MONGO DB
            if (redisMenu == null) {
                LogServiceImpl.Log(
                        this,
                        LogLevel.INFO,
                        (rq.getMsisdn() + "|****** GETTING PRINCIPAL FROM MONGODB FOR ******|").toUpperCase());
                menuPrincipale = (List<MenuPrincipaleCollection>) this.menuPrincipalServiceIpml.getMenu();

                if (menuPrincipale.size() == 0) {
                    LogServiceImpl.Log(
                            this,
                            LogLevel.INFO,
                            (rq.getMsisdn() + "|****** GETTING PRINCIPAL FROM POSTGRESQL DB FOR ******").toUpperCase());

                    String menuPostgres = this.helper.getPrincipalMenuFromPostgres().getValue();
                    ObjectMapper objectMapper = new ObjectMapper();

                    // MENU GOT FROM REDIS CACHE
                    menuPrincipale = objectMapper.readValue(
                            menuPostgres
                            ,
                            new TypeReference<List<MenuPrincipaleCollection>>(){}
                    );

                    if (menuPrincipale.size() == 0) {
                        //TODO READ MENU FROM ORACLE SEPARATED APPLICATION
                        // Not data on postgres
                        menu.append("Nous rencontrons une erreur serveur, veillez reessayez dans quelques instants svp.\n");
                    } else {
                        menuPrincipale.forEach(
                                t -> {
                                    if (t.getPosition() == 0) {
                                        menu.append(t.getName()).append("\n");
                                    } else {
                                        menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                                    }
                                }
                        );
                    }
                }
                // MENU GOT FROM MONGO DATABASE
                else {
                    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                    String updaterJsonString = ow.writeValueAsString(menuPrincipale);
                    updaterJsonString = new Minify().minify(updaterJsonString);

                    this.redisCacheRepository.addMenuPrincipal(updaterJsonString);

                    menuPrincipale.forEach(
                            t -> {
                                if (t.getPosition() == 0) {
                                    menu.append(t.getName()).append("\n");
                                } else {
                                    menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                                }
                            }
                    );
                }
            }
            else {
                LogServiceImpl.Log(
                        this,
                        LogLevel.INFO,
                        (rq.getMsisdn() + "|****** GETTING PRINCIPAL FROM REDIS CACHE FOR ******|").toUpperCase());
                ObjectMapper objectMapper = new ObjectMapper();

                // MENU GOT FROM REDIS CACHE
                menuPrincipale = objectMapper.readValue(
                        redisMenu
                        ,
                        new TypeReference<List<MenuPrincipaleCollection>>(){}
                );
                menuPrincipale.forEach(
                        t -> {
                            if (t.getPosition() == 0) {
                                menu.append(t.getName()).append("\n");
                            } else {
                                menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                            }
                        }
                );
            }

        }
        catch (IOException e) {
            LogServiceImpl.Log(
                    this,
                    LogLevel.INFO,
                    (rq.getMsisdn() + "|****** GETTING PRINCIPAL FROM MONGODB FOR ******").toUpperCase());

            menuPrincipale = (List<MenuPrincipaleCollection>) this.menuPrincipalServiceIpml.getMenu();

            if (menuPrincipale.size() == 0) {
                LogServiceImpl.Log(
                        this,
                        LogLevel.INFO,
                        (rq.getMsisdn() + "|****** GETTING PRINCIPAL FROM POSTGRESQL DB FOR ******").toUpperCase());

                String menuPostgres = this.helper.getPrincipalMenuFromPostgres().getValue();
                ObjectMapper objectMapper = new ObjectMapper();

                // MENU GOT FROM REDIS CACHE
                try {
                    menuPrincipale = objectMapper.readValue(
                            menuPostgres
                            ,
                            new TypeReference<List<MenuPrincipaleCollection>>(){}
                    );
                } catch (JsonProcessingException jsonProcessingException) {
                    jsonProcessingException.printStackTrace();
                }

                if (menuPrincipale.size() == 0) {
                    //TODO READ MENU FROM ORACLE SEPARATED APPLICATION
                    // Not data on postgres
                    menu.append("Nous rencontrons une erreur serveur, veillez reessayez dans quelques instants svp.\n");
                } else {
                    menuPrincipale.forEach(
                            t -> {
                                if (t.getPosition() == 0) {
                                    menu.append(t.getName()).append("\n");
                                } else {
                                    menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                                }
                            }
                    );
                }
            }
            // MENU GOT FROM MONGO DATABASE
            else {
                menuPrincipale.forEach(
                        t -> {
                            if (t.getPosition() == 0) {
                                menu.append(t.getName()).append("\n");
                            } else {
                                menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                            }
                        }
                );
            }
            e.printStackTrace();
        }
    }

    public String getDailyMenu(request rq, RedisCacheModel cache) {
        StringBuilder menu = new StringBuilder();

        LogServiceImpl.Log(
                this,
                LogLevel.INFO,
                (rq.getMsisdn() + "|****** GETTING DAILY BUNDLE ******").toUpperCase());

        List<MenuItemCollection> menuItems = new ArrayList<>();
        for (SubMenuCollection t: this.menuPrincipale.get(1).getSubMenu()) {
            if (t.getPosition() == 0) {
                menu.append(t.getName()).append("\n");
            } else {
                menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                menuItems.add(
                        new MenuItemCollection(
                                null,
                                t.getPosition(),
                                t.getName(),
                                t.getPrice(),
                                t.getKeyword(),
                                t.getMomoKeyword(),
                                t.getSi(),
                                t.getSub2eMenu()
                        )
                );
            }
        }
        cache.setMenuItems(menuItems);

        cache.freeFlow = "FC";
        cache.driven = null;
        menu.append("# Retour");
        return menu.toString();
    }

    public String get48hMenu(request rq, RedisCacheModel cache) {
        LogServiceImpl.Log(
                this,
                LogLevel.INFO,
                (rq.getMsisdn() + "|****** GETTING 48H BUNDLE ******").toUpperCase());

        StringBuilder menu = new StringBuilder();

        LogServiceImpl.Log(
                this,
                LogLevel.INFO,
                (rq.getMsisdn() + "|****** GETTING 48H BUNDLE ******").toUpperCase());

        List<MenuItemCollection> menuItems = new ArrayList<>();
        for (SubMenuCollection t: this.menuPrincipale.get(2).getSubMenu())  {
            if (t.getPosition() == 0) {
                menu.append(t.getName()).append("\n");
            } else {
                menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                menuItems.add(
                        new MenuItemCollection(
                                null,
                                t.getPosition(),
                                t.getName(),
                                t.getPrice(),
                                t.getKeyword(),
                                t.getMomoKeyword(),
                                t.getSi(),
                                t.getSub2eMenu()
                        )
                );
            }
        }


        cache.setMenuItems(menuItems);
        cache.driven = null;
        menu.append("# Retour");
        return menu.toString();
    }

    public void getDrivenCode(request rq, String drivenCode, RedisCacheModel cache) {
        LogServiceImpl.Log(
                this,
                LogLevel.INFO,
                (rq.getMsisdn() + "|****** REDIRECTING TO APP WITH DRIVENCODE " + drivenCode + "******").toUpperCase());
        cache.msg = null;
        cache.driven = drivenCode;
        cache.freeFlow = "FB";
    }

    public String getWeeklyMenu(request rq, RedisCacheModel cache) {
        StringBuilder menu = new StringBuilder();

        LogServiceImpl.Log(
                this,
                LogLevel.INFO,
                (rq.getMsisdn() + "|****** GETTING WEEKLY BUNDLE ******").toUpperCase());

        List<MenuItemCollection> menuItems = new ArrayList<>();
        for (SubMenuCollection t: this.menuPrincipale.get(4).getSubMenu())  {
            if (t.getPosition() == 0) {
                menu.append(t.getName()).append("\n");
            }
            else {
                menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                menuItems.add(
                        new MenuItemCollection(
                                null,
                                t.getPosition(),
                                t.getName(),
                                t.getPrice(),
                                t.getKeyword(),
                                t.getMomoKeyword(),
                                t.getSi(),
                                t.getSub2eMenu()
                        )
                );
            }
        }

        cache.setMenuItems(menuItems);
        cache.freeFlow = "FC";
        cache.driven = null;
        menu.append("# Retour");
        return menu.toString();
    }

    public String getMonthlyMenu(request rq, RedisCacheModel cache) {
        StringBuilder menu = new StringBuilder();

        LogServiceImpl.Log(
                this,
                LogLevel.INFO,
                (rq.getMsisdn() + "|****** GETTING MONTHLY BUNDLE ******").toUpperCase());

        List<MenuItemCollection> menuItems = new ArrayList<>();
        for (SubMenuCollection t: this.menuPrincipale.get(5).getSubMenu()) {
            if (t.getPosition() == 0) {
                menu.append(t.getName()).append("\n");
            } else {
                menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                menuItems.add(
                        new MenuItemCollection(
                                null,
                                t.getPosition(),
                                t.getName(),
                                t.getPrice(),
                                t.getKeyword(),
                                t.getMomoKeyword(),
                                t.getSi(),
                                t.getSub2eMenu()
                        )
                );
            }
        }

        cache.setMenuItems(menuItems);
        cache.freeFlow = "FC";
        cache.driven = null;
        menu.append("# Retour");
        return menu.toString();
    }

    public String getNightMenu(request rq, RedisCacheModel cache) {
        StringBuilder menu = new StringBuilder();

        LogServiceImpl.Log(
                this,
                LogLevel.INFO,
                (rq.getMsisdn() + "|****** GETTING NIGHT BUNDLE ******").toUpperCase());

        List<MenuItemCollection> menuItems = new ArrayList<>();
        for (SubMenuCollection t: this.menuPrincipale.get(6).getSubMenu()) {
            if (t.getPosition() == 0) {
                menu.append(t.getName()).append("\n");
            } else {
                menu.append(t.getPosition() + ". " + t.getName() + "\n");
                menuItems.add(
                        new MenuItemCollection(
                                null,
                                t.getPosition(),
                                t.getName(),
                                t.getPrice(),
                                t.getKeyword(),
                                t.getMomoKeyword(),
                                t.getSi(),
                                t.getSub2eMenu()
                        )
                );
            }
        }

        cache.setMenuItems(menuItems);
        cache.freeFlow = "FC";
        cache.driven = null;
        menu.append("# Retour");
        return menu.toString();
    }

    /**
     * Handling all Principal Menu choices
     * @param cache
     * @param rq
     * MENU CONTENT CHOICE TO HANDLE.
     * 1. Pass jour
     * 2. Pass 48H
     * 3. BONUS PASS MoMo
     * 4. Pass Semaine
     * 5. Pass Mois
     * 6. Pass Nuit
     * 7. YelloYa TV
     * 8. Mon compte
     */
    private void getPrincipalMenuChoice(RedisCacheModel cache, @RequestBody request rq) {
        switch (cache.choice) {
            // Daily Bundle
            case 1:
                cache.driven = null;
                cache.msg = this.getDailyMenu(rq, cache);
                cache.setStep(3);
                cache .setMenu(MenuEnum.MENU_24);
                break;

            // 48h bundles
            case 2:
                cache.driven = null;
                cache.msg = this.get48hMenu(rq, cache);
                cache.setStep(3);
                cache.setMenu(MenuEnum.MENU_48);
                break;

            // Driven Code to momo bundles aƒccelerate
            case 3:
            {
                this.getDrivenCode(rq, DRIVEN_MOMO_ACCELERATE_CODE, cache);
                this.helper.deleteCache(rq.getSessionId());
            }
            break;

            // Weekly bundles
            case 4:
                cache.driven = null;
                cache.msg = this.getWeeklyMenu(rq, cache);
                cache.setStep(3);
                cache .setMenu(MenuEnum.MENU_WEEKLY);
                break;
            // Monthly bundles
            case 5:
                cache.driven = null;
                cache.msg = this.getMonthlyMenu(rq, cache);
                cache.setStep(3);
                cache .setMenu(MenuEnum.MENU_MONTHLY);
                break;
            // Go nightly bundle
            case 6:
                cache.driven = null;
                cache.msg = this.getNightMenu(rq, cache);
                cache.setStep(3);
                cache .setMenu(MenuEnum.MENU_NIGHT);
                break;

            case 7:
                // GO to YelloYa TV
                this.getDrivenCode(rq, DRIVEN_MON_COMPTE_CODE,  cache);
                this.helper.deleteCache(rq.getSessionId());
                break;

            case 8:
                // Go to Mon compte
                this.getDrivenCode(rq, DRIVEN_MON_COMPTE_CODE,  cache);
                this.helper.deleteCache(rq.getSessionId());
                break;

            default: cache.msg = this.getPrincipaleMenu(rq, cache);
                cache.setMenu(MenuEnum.MENU_PRINCIPAL);
        }
    }

    /**
     * This method handle all principal menu options
     * @param rq
     * @param cache
     */
    private void getMenuSelectedByUser(@RequestBody request rq, RedisCacheModel cache) {
        // PRINCIPAL MENU OPTIONS
        StringBuilder menu = new StringBuilder();
        if (cache.getMenu().equals(MenuEnum.MENU_24)) {
            switch (rq.getSubscriberInput().trim()) {
                case "1":
                {
                    cache.choice= 1;

                    List<MenuItemCollection> menuItems = new ArrayList<>();
                    for (SubMenuCollection t: this.getMenuPrincipale().get(1)
                            .getSubMenu()
                            .get(1)
                            .getSub2eMenu())
                    {
                        if (t.getPosition() == 0) {
                            menu.append(t.getName()).append("\n\n");
                        } else {
                            menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                            menuItems.add(
                                    new MenuItemCollection(
                                            null,
                                            t.getPosition(),
                                            t.getName(),
                                            t.getPrice(),
                                            t.getKeyword(),
                                            t.getMomoKeyword(),
                                            t.getSi(),
                                            t.getSub2eMenu()
                                    )
                            );
                        }
                    }

                    cache.setMenuItems(menuItems);
                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_24);
                    cache.setStep(4);

                } break;
                case "2":
                {
                    cache.choice= 2;
                    List<MenuItemCollection> menuItems = new ArrayList<>();
                    for (SubMenuCollection t: this.getMenuPrincipale().get(1)
                            .getSubMenu()
                            .get(2)
                            .getSub2eMenu())
                    {
                        if (t.getPosition() == 0) {
                            menu.append(t.getName()).append("\n");
                        } else {
                            menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                            menuItems.add(
                                    new MenuItemCollection(
                                            null,
                                            t.getPosition(),
                                            t.getName(),
                                            t.getPrice(),
                                            t.getKeyword(),
                                            t.getMomoKeyword(),
                                            t.getSi(),
                                            t.getSub2eMenu()
                                    )
                            );
                        }
                    }

                    cache.setMenuItems(menuItems);
                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_24);
                    cache.setStep(4);
                }
                break;
                case "3":
                {
                    cache.choice= 3;
                    List<MenuItemCollection> menuItems = new ArrayList<>();
                    for (SubMenuCollection t: this.getMenuPrincipale().get(1)
                            .getSubMenu()
                            .get(3)
                            .getSub2eMenu())
                    {
                        if (t.getPosition() == 0) {
                            menu.append(t.getName()).append("\n\n");
                        } else {
                            menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                            menuItems.add(
                                    new MenuItemCollection(
                                            null,
                                            t.getPosition(),
                                            t.getName(),
                                            t.getPrice(),
                                            t.getKeyword(),
                                            t.getMomoKeyword(),
                                            t.getSi(),
                                            t.getSub2eMenu()
                                    )
                            );
                        }
                    }

                    cache.setMenuItems(menuItems);
                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_24);
                    cache.setStep(4);
                }
                break;
                case "4":
                {
                    cache.choice= 4;

                    List<MenuItemCollection> menuItems = new ArrayList<>();
                    for (SubMenuCollection t: this.getMenuPrincipale().get(1)
                            .getSubMenu()
                            .get(4)
                            .getSub2eMenu())
                    {
                        if (t.getPosition() == 0) {
                            menu.append(t.getName()).append("\n\n");
                        } else {
                            menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                            menuItems.add(
                                    new MenuItemCollection(
                                            null,
                                            t.getPosition(),
                                            t.getName(),
                                            t.getPrice(),
                                            t.getKeyword(),
                                            t.getMomoKeyword(),
                                            t.getSi(),
                                            t.getSub2eMenu()
                                    )
                            );
                        }
                    }

                    cache.setMenuItems(menuItems);
                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_24);
                    cache.setStep(4);
                }
                break;
                case "5":
                {
                    cache.choice= 5;
                    List<MenuItemCollection> menuItems = new ArrayList<>();
                    for (SubMenuCollection t: this.getMenuPrincipale().get(1)
                            .getSubMenu()
                            .get(5)
                            .getSub2eMenu()
                    )
                    {
                        if (t.getPosition() == 0) {
                            menu.append(t.getName()).append("\n");
                        } else {
                            menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                            menuItems.add(
                                    new MenuItemCollection(
                                            null,
                                            t.getPosition(),
                                            t.getName(),
                                            t.getPrice(),
                                            t.getKeyword(),
                                            t.getMomoKeyword(),
                                            t.getSi(),
                                            t.getSub2eMenu()
                                    )
                            );
                        }
                    }

                    menu.append("# Retour\n");
                    cache.setMenuItems(menuItems);
                    cache.setMenu(MenuEnum.MENU_24_BONUS_MOMO);
                    cache.setStep(4);
                }
                break;
                default: {
                    menu.append(this.getDailyMenu(rq, cache));
                    cache.step = 3;
                    cache.menu = MenuEnum.MENU_24;
                }
            }
        }
        else if (cache.getMenu().equals(MenuEnum.MENU_48)) {
            switch (rq.getSubscriberInput().trim()) {
                case "1":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(2)
                            .getSubMenu()
                            .get(1);
                    // GETING CONFIRMATION MENU
                    menu.append(
                            this.helper.getConfirmationString(confirmationMenu, " valide 48H ", "(15MB Bonus)")
                    );
                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_48);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                }
                break;
                case "2":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(2)
                            .getSubMenu()
                            .get(2);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 48H ",
                                    "(35MB Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_48);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                }
                break;
                case "3":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(2)
                            .getSubMenu()
                            .get(3);
                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 48H ",
                                    "(50% Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_48);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                }
                break;
                case "4":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(2)
                            .getSubMenu()
                            .get(4);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 48H ",
                                    "(50% Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_48);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                }
                break;
                case "5":
                {
                    List<MenuItemCollection> menuItems = new ArrayList<>();
                    for (SubMenuCollection t: this.getMenuPrincipale().get(2)
                            .getSubMenu()
                            .get(5)
                            .getSub2eMenu())
                    {
                        if (t.getPosition() == 0) {
                            menu.append(t.getName()).append("\n");
                        } else {
                            menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                            menuItems.add(
                                    new MenuItemCollection(
                                            null,
                                            t.getPosition(),
                                            t.getName(),
                                            t.getPrice(),
                                            t.getKeyword(),
                                            t.getMomoKeyword(),
                                            t.getSi(),
                                            t.getSub2eMenu()
                                    )
                            );
                        }
                    }

                    cache.setMenuItems(menuItems);
                    cache.setMenu(MenuEnum.MENU_48_BONUS_MOMO);
                    cache.setStep(4);
                    menu.append("# Retour\n");
                }
                break;
                default: {
                    menu.append(this.get48hMenu(rq, cache));
                    cache.step = 3;
                    cache.menu = MenuEnum.MENU_48;
                }
            }
        }
        else if (cache.getMenu().equals(MenuEnum.MENU_WEEKLY)) {
            switch (rq.getSubscriberInput().trim()) {
                case "1":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(4)
                            .getSubMenu()
                            .get(1);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 7 jours ",
                                    "(50% Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_WEEKLY);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                }
                break;
                case "2":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(4)
                            .getSubMenu()
                            .get(2);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 7 jours ",
                                    "(50% Bonus)"
                            )
                    );
                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_WEEKLY);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                }
                break;
                case "3":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(4)
                            .getSubMenu()
                            .get(3);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 7 jours ",
                                    "(50% Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_WEEKLY);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                }
                break;
                case "4":
                {
                    List<MenuItemCollection> menuItems = new ArrayList<>();
                    for (SubMenuCollection t: this.getMenuPrincipale().get(4)
                            .getSubMenu()
                            .get(4)
                            .getSub2eMenu())
                    {
                        if (t.getPosition() == 0) {
                            menu.append(t.getName()).append("\n");
                        } else {
                            menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                            menuItems.add(
                                    new MenuItemCollection(
                                            null,
                                            t.getPosition(),
                                            t.getName(),
                                            t.getPrice(),
                                            t.getKeyword(),
                                            t.getMomoKeyword(),
                                            t.getSi(),
                                            t.getSub2eMenu()
                                    )
                            );
                        }
                    }


                    menu.append("# Retour\n");
                    cache.setMenuItems(menuItems);
                    cache.setMenu(MenuEnum.MENU_WEEKLY_BONUS_MOMO);
                    cache.setStep(4);
                }
                break;
                default: {
                    menu.append(this.getWeeklyMenu(rq, cache));
                    cache.step = 3;
                    cache.menu = MenuEnum.MENU_WEEKLY;
                }
            }
        }
        else if (cache.getMenu().equals(MenuEnum.MENU_MONTHLY)) {
            switch (rq.getSubscriberInput().trim()) {
                case "1":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(5)
                            .getSubMenu()
                            .get(1);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 1 mois ",
                                    "(50% Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_MONTHLY);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                    break;
                }
                case "2":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(5)
                            .getSubMenu()
                            .get(2);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 1 mois ",
                                    "(50% Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_MONTHLY);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                    break;
                }
                case "3":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(5)
                            .getSubMenu()
                            .get(3);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 1 mois ",
                                    "(50% Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_MONTHLY);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                    break;
                }
                case "4":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(5)
                            .getSubMenu()
                            .get(4);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 1 mois ",
                                    "(50% Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_MONTHLY);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                    break;
                }
                case "5":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(5)
                            .getSubMenu()
                            .get(5);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 1 mois ",
                                    "(50% Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_MONTHLY);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                    break;
                }
                case "6":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(5)
                            .getSubMenu()
                            .get(6);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 1 mois ",
                                    "(50% Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_MONTHLY);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                    break;
                }
                case "7":
                {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(5)
                            .getSubMenu()
                            .get(7);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide 1 mois ",
                                    "(50% Bonus)"
                            )
                    );

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_MONTHLY);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                    break;
                }
                case "8":
                {
                    List<MenuItemCollection> menuItems = new ArrayList<>();
                    for (SubMenuCollection t: this.getMenuPrincipale().get(5)
                            .getSubMenu()
                            .get(8)
                            .getSub2eMenu())
                    {
                        if (t.getPosition() == 0) {
                            menu.append(t.getName()).append("\n");
                        } else {
                            menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                            menuItems.add(
                                    new MenuItemCollection(
                                            null,
                                            t.getPosition(),
                                            t.getName(),
                                            t.getPrice(),
                                            t.getKeyword(),
                                            t.getMomoKeyword(),
                                            t.getSi(),
                                            t.getSub2eMenu()
                                    )
                            );
                        }
                    }

                    cache.setMenuItems(menuItems);
                    cache.setMenu(MenuEnum.MENU_MONTHLY_BONUS_MOMO);
                    cache.setStep(4);
                    menu.append("# Retour\n");
                }
                break;
                default: {
                    menu.append(this.getMonthlyMenu(rq, cache));
                    cache.step = 3;
                    cache.menu = MenuEnum.MENU_MONTHLY;
                }
            }
        }
        else if (cache.getMenu().equals(MenuEnum.MENU_NIGHT)) {
            switch (rq.getSubscriberInput().trim()) {
                case "1": {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(6)
                            .getSubMenu()
                            .get(1);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide de 23h a 6h ",
                                    "(50% Bonus)"
                            )
                    );


                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_NIGHT);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                    break;
                }
                case "2": {
                    SubMenuCollection confirmationMenu = this.getMenuPrincipale().get(6)
                            .getSubMenu()
                            .get(2);

                    menu.append(
                            this.helper.getConfirmationString(
                                    confirmationMenu,
                                    " valide de 23h a 6h ",
                                    "(50% Bonus)"
                            )
                    );


                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_NIGHT);
                    cache.setConfirmationMenu(confirmationMenu);
                    cache.setStep(4);
                    break;
                }
                default: {
                    menu.append(this.getNightMenu(rq, cache));
                    cache.step = 3;
                    cache.menu = MenuEnum.MENU_NIGHT;
                }
            }
        }

        cache.msg = menu.toString();
    }

    /**
     * This method handle all application step
     * */
    public void stepHandler(@RequestBody request rq, RedisCacheModel cache) {
        if (cache.step == 2) {
            StringBuilder menu = new StringBuilder();
            if (!rq.getSubscriberInput().trim().equals("#")) {
                try {
                    cache.choice = Integer.parseInt(rq.getSubscriberInput().trim());
                    // PRINCIPAL MENU CHOICES
                    getPrincipalMenuChoice(cache, rq);
                }
                catch (Exception e) {
                    LogServiceImpl.Log(
                            this,
                            LogLevel.INFO,
                            (rq.getMsisdn() + "|****** USER INPUT NOT AN INTEGER  ******").toUpperCase());


                    menu = new StringBuilder().append("MTN INTERNET\n");
                    menu.append("Veillez saisir un entier valide.");

                    cache.driven = null;
                    menu.append("# Retour");
                    cache.msg = menu.toString() ;
                    cache.freeFlow = "FC";

                    e.printStackTrace();
                }
            }
            //User input is for back
            else {
                cache.msg = this.getPrincipaleMenu(rq, cache);
                cache.setStep(2);
                cache.setMenu(MenuEnum.MENU_PRINCIPAL);
            }
        }
        else if (cache.step == 3) {
            if (rq.getSubscriberInput().trim().equals("#")) {
                cache.msg = this.getPrincipaleMenu(rq, cache);
                cache.setStep(2);
                cache .setMenu(MenuEnum.MENU_PRINCIPAL);
            } else {
                // Getting User selection
                getMenuSelectedByUser(rq, cache);
            }
        }
        else if (cache.step == 4) {
            // WHEN USER PUT # to BACK to previouse MENU
            StringBuilder menu = new StringBuilder();
            if (rq.getSubscriberInput().trim().equals("#")) {
                switch (cache.getMenu()) {
                    case MENU_24:
                    case MENU_24_BONUS_MOMO:
                    case MENU_CONFIRMATION_24:
                        cache.msg = this.getDailyMenu(rq, cache);
                        cache.setStep(3);
                        cache.setMenu(MenuEnum.MENU_24);
                        break;
                    case MENU_48:
                    case MENU_48_BONUS_MOMO:
                    case MENU_CONFIRMATION_48:
                        cache.msg = this.get48hMenu(rq, cache);
                        cache.setStep(3);
                        cache.setMenu(MenuEnum.MENU_48);
                        break;
                    case MENU_WEEKLY:
                    case MENU_WEEKLY_BONUS_MOMO:
                    case MENU_CONFIRMATION_WEEKLY:
                        cache.msg = this.getWeeklyMenu(rq, cache);
                        cache.setStep(3);
                        cache.setMenu(MenuEnum.MENU_WEEKLY);
                        break;
                    case MENU_MONTHLY:
                    case MENU_MONTHLY_BONUS_MOMO:
                    case MENU_CONFIRMATION_MONTHLY:
                        cache.msg = this.getMonthlyMenu(rq, cache);
                        cache.setStep(3);
                        cache.setMenu(MenuEnum.MENU_MONTHLY);
                        break;
                    case MENU_NIGHT:
                    case MENU_CONFIRMATION_NIGHT:
                        cache.msg = this.getNightMenu(rq, cache);
                        cache.setStep(3);
                        cache.setMenu(MenuEnum.MENU_NIGHT);
                        break;
                    default:
                        cache.msg = this.getPrincipaleMenu(rq, cache);
                        cache.setStep(2);
                        cache.setMenu(MenuEnum.MENU_PRINCIPAL);
                }
            }
            // WHEN USER CHOOSE PAYMENT OPTIONS
            else {
                if (
                        cache.getMenu().equals(MenuEnum.MENU_24_BONUS_MOMO) ||
                                cache.getMenu().equals(MenuEnum.MENU_48_BONUS_MOMO) ||
                                cache.getMenu().equals(MenuEnum.MENU_WEEKLY_BONUS_MOMO) ||
                                cache.getMenu().equals(MenuEnum.MENU_MONTHLY_BONUS_MOMO)
                )
                {
                    MenuItemCollection bundle = new MenuItemCollection();
                    for (MenuItemCollection m : cache.getMenuItems())  {
                        if (rq.getSubscriberInput().trim().equals(String.valueOf(m.getPosition()))) {
                            bundle = m;
                        }
                    }
                    if (bundle.getPosition() != 0) {

                        LogServiceImpl.Log(
                                this,
                                LogLevel.INFO,
                                (rq.getMsisdn() + "|****** MOMO BONUS BUNDLE SENDING REQUEST TO ECW WITH KEYWORD: "+ bundle.getKeyword() +"  and price "+ bundle.getPrice() +" GNF ******").toUpperCase());

                        menu.append(this.helper.sendRequestToMoMo(
                                bundle.getPrice(),
                                bundle.getKeyword(),
                                rq.getMsisdn()
                        ));
                        cache.msg = menu.toString();
                        cache.freeFlow = "FB";
                    }
                }
                /**
                 * HANDLE ADD_ON CONFIRMATION
                 */
                else if (cache.getMenu().equals(MenuEnum.MENU_CONFIRMATION_24)) {

                    for(MenuItemCollection f : cache.getMenuItems()) {
                        if (String.valueOf(f.getPosition()).equals(rq.getSubscriberInput().trim())) {
                            String addOn = "";
                            if (f.getName().toLowerCase().contains("40mb")) {
                                addOn = "(10MB Bonus)";
                            }
                            else if(f.getName().toLowerCase().contains("120mb")){
                                addOn = "(30MB Bonus)";
                            }
                            else if(f.getName().toLowerCase().contains("250mb")){
                                addOn = "(50%)";
                            }
                            else if(f.getPosition() == 4){
                                addOn = "(50%)";
                            }
                            menu.append(
                                    this.helper.getConfirmation24(f, addOn)
                            );

                            SubMenuCollection confirmationMenu = new SubMenuCollection();
                            confirmationMenu.setPosition(f.getPosition());
                            confirmationMenu.setName(f.getName());
                            confirmationMenu.setKeyword(f.getKeyword());
                            confirmationMenu.setMomoKeyword(f.getMomoKeyword());
                            confirmationMenu.setPrice(f.getPrice());

                            cache.setConfirmationMenu(confirmationMenu);
                        }
                    }

                    cache.setMenu(MenuEnum.MENU_CONFIRMATION_24_FINAL_STEP);
                    cache.setStep(5);
                }

                else if (
                        cache.getMenu().equals(MenuEnum.MENU_CONFIRMATION_48) ||
                                cache.getMenu().equals(MenuEnum.MENU_CONFIRMATION_WEEKLY) ||
                                cache.getMenu().equals(MenuEnum.MENU_CONFIRMATION_MONTHLY) ||
                                cache.getMenu().equals(MenuEnum.MENU_CONFIRMATION_NIGHT)

                ){
                    menu.append(
                            this.helper.sendPayment(rq, cache.getConfirmationMenu(), cache)
                    );
                }
                cache.msg = menu.toString();
            }
        }
        else if (cache.step == 5) {
            StringBuilder menu = new StringBuilder();

            if (rq.getSubscriberInput().trim().equals("#")) {
                cache.step = 3;
                cache.menu = MenuEnum.MENU_24;
                cache.msg = this.getDailyMenu(rq, cache);
            }
            else {
                menu.append(
                        this.helper.sendPayment(rq, cache.getConfirmationMenu(), cache)
                );
                cache.msg = menu.toString();
                cache.step = 6;
                cache.freeFlow = "FB";
                cache.driven = null;
            }
        }


        this.helper.saveCache(cache);
    }


}
