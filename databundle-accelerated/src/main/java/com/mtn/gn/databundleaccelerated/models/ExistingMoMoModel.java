package com.mtn.gn.databundleaccelerated.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExistingMoMoModel {
    private String msisdn;
    private boolean exist;
    private String comment;
}
