package com.mtn.gn.databundleaccelerated.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mtn.gn.databundleaccelerated.collections.SubMenuCollection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuItemCollection implements Serializable {
    @JsonIgnore
    private String comment;
    private int position;
    private String name;
    private int price;
    private String keyword;
    private String momoKeyword;
    private String si;
    private List<SubMenuCollection> subMenu;
}
