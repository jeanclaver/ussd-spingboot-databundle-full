package com.mtn.gn.databundleaccelerated.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mtn.gn.databundleaccelerated.collections.DataBundleMenuUpdater;
import com.mtn.gn.databundleaccelerated.collections.MenuPrincipaleCollection;
import com.mtn.gn.databundleaccelerated.entities.MenuPrincipalEntity;
import com.mtn.gn.databundleaccelerated.models.MenuPrincipalModel;
import com.mtn.gn.databundleaccelerated.proxies.UnirestServiceProxy;
import com.mtn.gn.databundleaccelerated.repositories.MenuPrincipalFromPostgresRepository;
import com.mtn.gn.databundleaccelerated.repositories.MenuPrincipaleRepository;
import com.mtn.gn.databundleaccelerated.repositories.RedisCacheRepository;
import com.mtn.gn.databundleaccelerated.repositories.UpdaterRepository;
import com.mtn.gn.databundleaccelerated.services.UpdateService;
import com.mtn.gn.databundleaccelerated.utils.Minify;
import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.services.LogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UpdateServiceImpl implements UpdateService {
    private List<DataBundleMenuUpdater> updaterData;

    @Autowired
    private MenuPrincipaleRepository menuPrincipaleRepository;

    @Autowired
    private MenuPrincipalFromPostgresRepository menuPrincipalFromPostgresRepository;

    @Autowired
    private RedisCacheRepository redisCacheRepository;

    @Autowired
    private UpdaterRepository updaterRepository;

    @Autowired
    UnirestServiceProxy unirestServiceProxy;


    @Override
    public boolean updateRedis() {
        this.updaterData = (List<DataBundleMenuUpdater>) this.updaterRepository.findAllByOrderByPosition();

        if (this.updaterData.size() > 0) {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            try {
                String json = ow.writeValueAsString(updaterData);
                this.redisCacheRepository.addMenuPrincipal(new Minify().minify(json));
                return true;
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean updateMongo() {
        this.updaterData = (List<DataBundleMenuUpdater>) this.updaterRepository.findAllByOrderByPosition();
        if (this.updaterData.size() > 0) {
            this.menuPrincipaleRepository.deleteAll();
            for(DataBundleMenuUpdater m: this.updaterData){
                this.menuPrincipaleRepository.save(
                        new MenuPrincipaleCollection(
                                null,
                                m.getPosition(),
                                m.getName(),
                                m.getComment(),
                                m.getSubMenu()
                        )
                );
            }
            LogServiceImpl.Log(
                    this,
                    LogLevel.INFO,
                    ("****** MENU MONGO DB UPDATED  ******").toUpperCase());
            return true;
        }
        return false;
    }

    @Override
    public boolean updatePostgres() {
        this.updaterData = (List<DataBundleMenuUpdater>) this.updaterRepository.findAllByOrderByPosition();
        if (this.updaterData.size() > 0) {
            this.menuPrincipalFromPostgresRepository.deleteAll();

            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            try {
                String json = ow.writeValueAsString(updaterData);
                MenuPrincipalEntity resp = this.menuPrincipalFromPostgresRepository.save(new MenuPrincipalEntity("MENU", new Minify().minify(json)));

                if (resp.getValue().length() > 0) {
                    LogServiceImpl.Log(
                            this,
                            LogLevel.INFO,
                            ("****** MENU POSTGRES DB UPDATED  ******").toUpperCase());
                    return true;
                }
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean updateOracle() {
        this.updaterData = (List<DataBundleMenuUpdater>) this.updaterRepository.findAllByOrderByPosition();
        if (this.updaterData.size() > 0) {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String updaterJsonString = null;
            try {
                updaterJsonString = ow.writeValueAsString(updaterData);

                updaterJsonString = new Minify().minify(updaterJsonString);

                MenuPrincipalModel response = this.unirestServiceProxy.saveToOracleDB(new MenuPrincipalModel("MENU", updaterJsonString));

                if (response != null) {
                    LogServiceImpl.Log(
                            this,
                            LogLevel.INFO,
                            ("****** MENU ORACLE DB UPDATED  ******").toUpperCase());
                    return  true;
                }
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
