package com.mtn.gn.databundleaccelerated.collections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "databundle_updater")
public class DataBundleMenuUpdater {
    private String _id;
    private int position;
    private String name;
    @JsonIgnore
    private String comment;
    private List<SubMenuCollection> subMenu;

}
