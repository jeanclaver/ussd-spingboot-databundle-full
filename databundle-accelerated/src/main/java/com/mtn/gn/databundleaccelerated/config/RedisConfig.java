package com.mtn.gn.databundleaccelerated.config;

import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.services.LogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

import java.net.UnknownHostException;

@Configuration
public class RedisConfig {
    @Autowired
    RedisConfigRessources cR;

    @Bean
    public RedisConnectionFactory redisConnectionFactory() throws UnknownHostException {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(512);
        poolConfig.setMinIdle(20);
        poolConfig.setMaxIdle(20);

        JedisConnectionFactory factory = new JedisConnectionFactory(poolConfig);
        factory.setHostName(this.cR.getHost());
        factory.setUsePool(true);
        factory.setPort(this.cR.getPort());
        return factory;
    }

    //@Bean
    JedisConnectionFactory jedisConnectionFactory()  {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(cR.getHost(), cR.getPort());
        //redisStandaloneConfiguration.setPassword(RedisPassword.of("yourRedisPasswordIfAny"));
        return new JedisConnectionFactory(redisStandaloneConfiguration);
    }


    @Bean
    public RedisTemplate<?, ?> redisTemplate() {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        RedisSerializer<String> stringSerializer = new StringRedisSerializer();
        try {
            template.setConnectionFactory(redisConnectionFactory());
            template.setKeySerializer(stringSerializer);
            template.setHashKeySerializer(stringSerializer);
            template.setValueSerializer(stringSerializer);
            template.setHashValueSerializer(stringSerializer);
            template.setEnableTransactionSupport(true);
            template.afterPropertiesSet();
            return template;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }
}
