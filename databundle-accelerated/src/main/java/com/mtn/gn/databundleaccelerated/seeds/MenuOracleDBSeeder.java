package com.mtn.gn.databundleaccelerated.seeds;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mtn.gn.databundleaccelerated.collections.DataBundleMenuUpdater;
import com.mtn.gn.databundleaccelerated.models.MenuPrincipalModel;
import com.mtn.gn.databundleaccelerated.proxies.UnirestServiceProxy;
import com.mtn.gn.databundleaccelerated.repositories.UpdaterRepository;
import com.mtn.gn.databundleaccelerated.utils.Minify;
import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.services.LogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(3)
public class MenuOracleDBSeeder implements CommandLineRunner {


    @Autowired
    UnirestServiceProxy unirestServiceProxy;

    @Autowired
    UpdaterRepository updaterRepository;

    @Override
    public void run(String... args) throws Exception {
        Iterable<DataBundleMenuUpdater> updateMenu = this.updaterRepository.findAllByOrderByPosition();

        LogServiceImpl.Log(
                this,
                LogLevel.INFO,
                ("****** SAVING MENU TO ORACLE DB AT STARTING APPLICATION ******").toUpperCase());

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String updaterJsonString = ow.writeValueAsString(updateMenu);
        updaterJsonString = new Minify().minify(updaterJsonString);
        MenuPrincipalModel response ;

        try {
           response  = this.unirestServiceProxy.saveToOracleDB(new MenuPrincipalModel("MENU", updaterJsonString));
        } catch (Exception e) {
            LogServiceImpl.Log(
                    this,
                    LogLevel.INFO,
                    ("****** Oracle interface service is not up ******").toUpperCase());
            response = null;
        }

        if (response == null) {
            LogServiceImpl.Log(
                    this,
                    LogLevel.INFO,
                    ("****** FAILED TO MENU TO ORACLE DB AT STARTING APPLICATION ******").toUpperCase());
        }
    }

    public UpdaterRepository getUpdaterRepository() {
        return updaterRepository;
    }
}
