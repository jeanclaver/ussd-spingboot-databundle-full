package com.mtn.gn.databundleaccelerated.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateDatabasesModel {
    private boolean redis;
    private boolean mongo;
    private boolean postgres;
    private boolean oracle;
    private boolean file;
}
