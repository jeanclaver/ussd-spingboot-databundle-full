package com.mtn.gn.databundleaccelerated.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationModel {
    private String _id;
    private String processingNumber;
    private String momTransactionID;
    private String statusCode;
    private String statusDesc;
    private String thirdPartyAcctRef;
    private SubscriberInfoModel subscriberInfo;
    private String keyword;
    private String serviceName;
    private String statutMdp;
    private boolean statutServiceRequested;
}
