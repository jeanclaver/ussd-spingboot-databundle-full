package com.mtn.gn.databundleaccelerated.seeds;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mtn.gn.databundleaccelerated.collections.DataBundleMenuUpdater;
import com.mtn.gn.databundleaccelerated.entities.MenuPrincipalEntity;
import com.mtn.gn.databundleaccelerated.repositories.MenuPrincipalFromPostgresRepository;
import com.mtn.gn.databundleaccelerated.repositories.UpdaterRepository;
import com.mtn.gn.databundleaccelerated.services.impl.MenuPrincipalServiceIpml;
import com.mtn.gn.databundleaccelerated.utils.Minify;
import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.services.LogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(2)
public class MenuPostgresDBSeeder implements CommandLineRunner {

    @Autowired
    MenuPrincipalServiceIpml menuPrincipalServiceIpml;

    @Autowired
    MenuPrincipalFromPostgresRepository repository;

    @Autowired
    UpdaterRepository updaterRepository;

    @Override
    public void run(String... args) throws Exception {
        Iterable<DataBundleMenuUpdater> updateMenu = this.updaterRepository.findAllByOrderByPosition();

        LogServiceImpl.Log(
                this,
                LogLevel.INFO,
                ("****** SAVING MENU TO POSTGRESDB DB FROM STARTING ******").toUpperCase());

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String updaterJsonString = ow.writeValueAsString(updateMenu);
        updaterJsonString = new Minify().minify(updaterJsonString);

        this.menuPrincipalServiceIpml.saveMenuToOracle(new MenuPrincipalEntity("MENU", updaterJsonString));
    }
}
