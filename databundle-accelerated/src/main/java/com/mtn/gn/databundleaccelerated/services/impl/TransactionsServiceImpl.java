package com.mtn.gn.databundleaccelerated.services.impl;

import com.mtn.gn.databundleaccelerated.config.MyConfigProperties;
import com.mtn.gn.databundleaccelerated.models.DatabundleTransactionModel;
import com.mtn.gn.databundleaccelerated.proxies.UnirestServiceProxy;
import com.mtn.gn.databundleaccelerated.services.TransactionsService;
import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.services.LogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionsServiceImpl implements TransactionsService {
    @Autowired
    MyConfigProperties myConfigProperties;

    @Autowired
    UnirestServiceProxy unirestServiceProxy;

    @Override
    public boolean saveDatabundleTransaction(DatabundleTransactionModel data) {
        try  {
           DatabundleTransactionModel resp =  this.unirestServiceProxy.sendTransaction(data);
            LogServiceImpl.Log(
                    this,
                    LogLevel.INFO,
                    ("****** SAVING TRANSACTION TO ORACLE DB  ******\n"+ resp).toUpperCase());
            return true;

        } catch (Exception e) {
            LogServiceImpl.Log(
                    this,
                    LogLevel.ERROR,
                    ("****** CONNECTION ISSUE ON ORACLE DB  ******\n" + e.getMessage()).toUpperCase());

            e.printStackTrace();
            return false;
        }

    }
}
