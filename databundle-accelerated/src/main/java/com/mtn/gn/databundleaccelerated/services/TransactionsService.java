package com.mtn.gn.databundleaccelerated.services;

import com.mtn.gn.databundleaccelerated.models.DatabundleTransactionModel;

public interface TransactionsService {
    boolean saveDatabundleTransaction(DatabundleTransactionModel data);
}
