package com.mtn.gn.databundleaccelerated.collections;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "direct_short_codes")
public class DirectShortCodeCollection {
    @Id
    private String _id;
    private String name;
    private String price;
    private String keyword;
    private String momoKeyword;
}
