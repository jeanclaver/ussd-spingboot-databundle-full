package com.mtn.gn.databundleaccelerated.repositories;

import com.mtn.gn.databundleaccelerated.collections.DirectShortCodeCollection;
import org.springframework.data.repository.CrudRepository;

public interface DirectShortCodeRepository extends CrudRepository<DirectShortCodeCollection, String> {
    DirectShortCodeCollection findByPrice(int price);
}
