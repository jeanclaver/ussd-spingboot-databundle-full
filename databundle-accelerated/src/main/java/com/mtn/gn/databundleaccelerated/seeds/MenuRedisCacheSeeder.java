package com.mtn.gn.databundleaccelerated.seeds;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mtn.gn.databundleaccelerated.collections.DataBundleMenuUpdater;
import com.mtn.gn.databundleaccelerated.collections.MenuPrincipaleCollection;
import com.mtn.gn.databundleaccelerated.config.RunningEnvConfigRessources;
import com.mtn.gn.databundleaccelerated.repositories.MenuPrincipaleRepository;
import com.mtn.gn.databundleaccelerated.repositories.RedisCacheRepository;
import com.mtn.gn.databundleaccelerated.repositories.UpdaterRepository;
import com.mtn.gn.databundleaccelerated.services.impl.MenuPrincipalServiceIpml;
import com.mtn.gn.databundleaccelerated.utils.Minify;
import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.services.LogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

@Service
@Order(1)
public class MenuRedisCacheSeeder implements CommandLineRunner {
    @Autowired
    RedisCacheRepository redisCacheRepository;

    @Autowired
    MenuPrincipalServiceIpml menuPrincipalServiceIpml;


    @Autowired
    MenuPrincipaleRepository menuPrincipaleRepository;

    @Autowired
    UpdaterRepository updaterRepository;

    @Autowired
    RunningEnvConfigRessources runningEnvConfigRessources;

    @Override
    public void run(String... args) throws Exception {

        List<DataBundleMenuUpdater> updateMenu = (List<DataBundleMenuUpdater>) this.updaterRepository.findAllByOrderByPosition();

        if (updateMenu.size() == 0) {
            try {
                File file;

                if (runningEnvConfigRessources.getActive().equals("dev")) {
                  file   = new ClassPathResource("data/databundle_principale_menu.json").getFile();
                } else {
                    file = new File("/app/databundle_principale_menu.json");
                }

                String text = new String(Files.readAllBytes(file.toPath()));
                try {
                    LogServiceImpl.Log(
                            this,
                            LogLevel.INFO,
                            ("****** APPLICATION START WITH ADDING MENU TO REDIS CACHE AND TO UPDATER COLLECTION FROM FILE ******").toUpperCase());
                    this.redisCacheRepository.addMenuPrincipal(new Minify().minify(text));

                    // SAVING DATA TO LIVE COLLECTION
                    ObjectMapper objectMapper = new ObjectMapper();

                    // MENU GOT FROM REDIS CACHE
                    List<MenuPrincipaleCollection> menu = objectMapper.readValue(
                            text
                            ,
                            new TypeReference<List<MenuPrincipaleCollection>>(){}
                    );

                    this.menuPrincipaleRepository.deleteAll();
                    this.menuPrincipaleRepository.saveAll(menu);
                    // Mise a jour du updater par le fichier
                    this.updaterRepository.deleteAll();
                    menu.forEach(
                            f -> {
                                this.updaterRepository.save(
                                        new DataBundleMenuUpdater(
                                        null,
                                        f.getPosition(),
                                        f.getName(),
                                        f.getComment(),
                                        f.getSubMenu()
                                ));
                            });

                    LogServiceImpl.Log(
                            this,
                            LogLevel.INFO,
                            ("****** MENU PRINCIPAL ADDED TO CACHE FROM FILE ******").toUpperCase());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            LogServiceImpl.Log(
                    this,
                    LogLevel.INFO,
                    ("****** APPLICATION START WITH ADDING MENU TO REDIS CACHE FROM MONGO UPDATER COLLECTION ******").toUpperCase());

            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String updaterJsonString = ow.writeValueAsString(updateMenu);
            updaterJsonString = new Minify().minify(updaterJsonString);

            this.redisCacheRepository.addMenuPrincipal(updaterJsonString);

            this.menuPrincipaleRepository.deleteAll();
            updateMenu.forEach(
                    f -> {
                        this.menuPrincipaleRepository.save(
                                new MenuPrincipaleCollection(
                                        null,
                                        f.getPosition(),
                                        f.getName(),
                                        f.getComment(),
                                        f.getSubMenu()
                                ));
                    });

            LogServiceImpl.Log(
                    this,
                    LogLevel.INFO,
                    ("****** MENU PRINCIPAL ADDED TO CACHE FROM MONGO UPDATER COLLECTION ******").toUpperCase());
        }

    }
}
