package com.mtn.gn.databundleaccelerated.services.impl;

import com.mtn.gn.databundleaccelerated.collections.DirectShortCodeCollection;
import com.mtn.gn.databundleaccelerated.repositories.DirectShortCodeRepository;
import com.mtn.gn.databundleaccelerated.services.DirectShortCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DirectShortCodeServiceImpl implements DirectShortCodeService {
    @Autowired
    DirectShortCodeRepository directShortCodeRepository;

    @Override
    public DirectShortCodeCollection getShortCodeKeywords(int price) {
        return this.directShortCodeRepository.findByPrice(price);
    }

    @Override
    public List<DirectShortCodeCollection> getShortCodes() {
        return (List<DirectShortCodeCollection>) this.directShortCodeRepository.findAll();
    }
}
