package com.mtn.gn.databundleaccelerated.enums;

public enum  MenuConfirmation {
    MENU_24,
    MENU_48,
    MENU_WEEKLY,
    MENU_MONTHLY,
    MENU_NIGHT,
}
