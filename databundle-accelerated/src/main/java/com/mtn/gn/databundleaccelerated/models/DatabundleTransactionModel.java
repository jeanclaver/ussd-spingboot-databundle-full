package com.mtn.gn.databundleaccelerated.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DatabundleTransactionModel {
    private String transactionId;
    private String msisdn;
    private String serviceName;
    private int amount;
    private String keyword;
    private String status;
    private String createdAt;
}
