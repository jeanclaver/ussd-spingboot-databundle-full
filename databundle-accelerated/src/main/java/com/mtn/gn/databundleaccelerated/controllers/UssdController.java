package com.mtn.gn.databundleaccelerated.controllers;

import com.mtn.gn.databundleaccelerated.collections.DirectShortCodeCollection;
import com.mtn.gn.databundleaccelerated.config.MyConfigProperties;
import com.mtn.gn.databundleaccelerated.enums.MenuEnum;
import com.mtn.gn.databundleaccelerated.models.RedisCacheModel;
import com.mtn.gn.databundleaccelerated.models.xml.FreeFlowResponseModel;
import com.mtn.gn.databundleaccelerated.models.xml.request;
import com.mtn.gn.databundleaccelerated.models.xml.response;
import com.mtn.gn.databundleaccelerated.services.impl.DirectShortCodeServiceImpl;
import com.mtn.gn.databundleaccelerated.services.impl.HelperService;
import com.mtn.gn.databundleaccelerated.services.impl.UssdServiceHelper;
import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.services.LogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

@Controller
public class UssdController {
    @Autowired
    private UssdServiceHelper ussdServiceHelper;

    @Autowired
    private HelperService helper;

    @Autowired
    private DirectShortCodeServiceImpl directShortCodeService;

    @Autowired
    MyConfigProperties myConfigProperties;

    /**
     * HTTP REQUEST METHOD REQUEST HANDLER
     * @param rq
     * @return
     */
    @PostMapping(
            value = "/ussd",
            produces = {
                    "application/xml",
                    "text/xml"
            },
            consumes = {
                    "application/xml",
                    "text/xml"
            }
    )
    @ResponseBody
    public response ussd(@RequestBody request rq) {
        String msg = "";
        RedisCacheModel cache;

        if (rq.getNewRequest() != null
                && rq.getMsisdn() != null
                && rq.getSubscriberInput() != null
                && rq.getSessionId() != null
        ) {
            // FIRST REQUEST COMMING
            if (rq.getNewRequest().equals("1"))
            {
                LogServiceImpl.Log(
                        this,
                        LogLevel.INFO,
                        (rq.getMsisdn() + "|****** first request handling  ******").toUpperCase());

                cache = new RedisCacheModel(
                        2,
                        rq.getSessionId(),
                        0,
                        MenuEnum.MENU_PRINCIPAL,
                        rq.getMsisdn(),
                        msg,
                        null,
                        "FC",
                        new ArrayList<>(),
                        null,
                        null
                );

                if (rq.getSubscriberInput().trim().equals("#")) {
                    cache.msg = this.ussdServiceHelper.getPrincipaleMenu(rq, cache);
                    this.helper.saveCache(cache);
                }
                else {
                    String input = rq.getSubscriberInput();

                    String[] bits = input.split("\\*");
                    String inputValue = bits[bits.length-1];

                    try {
                        int response = Integer.parseInt(inputValue);

                        if (
                                response < 1000 ||
                                        inputValue.isEmpty()
                        ) {
                            cache.msg = this.ussdServiceHelper.getPrincipaleMenu(rq, cache);
                            this.helper.saveCache(cache);
                        }
                        // DIRECT SHORTCODE
                        else {
                            try {

                                DirectShortCodeCollection shortCode = this.directShortCodeService
                                        .getShortCodeKeywords(
                                                Integer.parseInt(inputValue)
                                        );
                                LogServiceImpl.Log(
                                        this,
                                        LogLevel.INFO,
                                        (rq.getMsisdn() + "|****** handling short code request ******\nshortcode" + shortCode + "\n input " + input ).toUpperCase());

                                if (shortCode == null) {
                                    cache.msg = "Cet achat direct n est pas configurer. MTN vous remercie pour votre fidelite\n";
                                    cache.freeFlow = "FB";
                                }
                                else {
                                    cache.msg = this.helper.directShortHandler(shortCode);
                                    cache.freeFlow = "FC";
                                    cache.menu = MenuEnum.MENU_CONFIRMATION_DIRECT_SHORT_CODE;
                                    cache.setDirectSc(shortCode);
                                    this.helper.saveCache(cache);
                                }
                            } catch (Exception e) {
                                e.getStackTrace();
                                cache.msg = "Cet achat direct n est pas configurer. MTN vous remercie pour votre fidelite\n";
                                cache.freeFlow = "FB";
                                LogServiceImpl.Log(
                                        this,
                                        LogLevel.ERROR,
                                        (rq.getMsisdn() + "|****** handling short code input ******\n" + e.getMessage()).toUpperCase());
                                e.printStackTrace();
                            }
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }

                this.checkMsgAndDriven(cache);
                return new response(
                        rq.getMsisdn(),
                        cache.msg,
                        null,
                        new FreeFlowResponseModel(
                                cache.freeFlow,
                                "N",
                                "0.0",
                                null
                        )
                );
            }
            else {
                //Getting information save in cache
                cache = this.helper.getCache(rq.getSessionId());

                if (cache == null) {
                    msg = "Erreur server, merci de reessayer plutard\n";
                    LogServiceImpl.Log(
                            this,
                            LogLevel.INFO,
                            (rq.getMsisdn() + "|****** THE is NewRequest must be 1 at the first request ******").toUpperCase());

                    return new response(
                            rq.getMsisdn(),
                            msg,
                            null,
                            new FreeFlowResponseModel(
                                    "FB",
                                    "N",
                                    "0.0",
                                    null
                            )

                    );
                }
                else {
                    // Handling Direct Short codes choice by MoMo or Credit.
                    if (cache.menu.equals(MenuEnum.MENU_CONFIRMATION_DIRECT_SHORT_CODE)) {
                        LogServiceImpl.Log(
                                this,
                                LogLevel.INFO,
                                (rq.getMsisdn() + "|****** Direct ShortCode Confirmation  ******").toUpperCase());

                        switch (rq.getSubscriberInput().trim()) {
                            case "1":
                                cache.msg = this.helper.sendRequestToMdp(
                                        cache.getDirectSc().getKeyword(),
                                        rq.getMsisdn(),
                                        Integer.parseInt(cache.getDirectSc().getPrice())
                                );
                                cache.freeFlow = "FB";
                                break;
                            case "2":
                                cache.msg = this.helper.sendRequestToMoMo(
                                        Integer.parseInt(cache.getDirectSc().getPrice()),
                                        cache.getDirectSc().getMomoKeyword(),
                                        rq.getMsisdn()
                                );
                                cache.freeFlow = "FB";
                                break;
                            case "#":
                                cache.step = 2;
                                cache.menu = MenuEnum.MENU_PRINCIPAL;
                                cache.msg = this.ussdServiceHelper.getPrincipaleMenu(rq, cache);
                                cache.freeFlow = "FC";
                                break;
                        }
                        this.helper.saveCache(cache);
                    }

                    // Differents steps handler
                    else {
                        // Normal Flow handler
                        this.ussdServiceHelper.stepHandler(rq, cache);
                    }

                    this.checkMsgAndDriven(cache);
                    response response = new response(
                            rq.getMsisdn(),
                            cache.msg,
                            cache.driven,
                            new FreeFlowResponseModel(
                                    cache.freeFlow,
                                    "N",
                                    "0.0",
                                    null
                            )
                    );
                    return response;
                }
            }
        }

        return new response(
                rq.getMsisdn(),
                "Veillez remplir tous les parametres requis",
                null,
                new FreeFlowResponseModel(
                        "FB",
                        "N",
                        "0.0",
                        null
                )

        );
    }

    private void checkMsgAndDriven(RedisCacheModel cache) {
        if (cache.driven == null && cache.msg == null) {
            cache.freeFlow = "FB";
            cache.msg = "Une erreur est survenue, veillez reessayer plus tard! MTN vous remercie pour votre fidelite\n";
        } else if (cache.driven != null && cache.msg != null) {
            cache.freeFlow = "FC";
            cache.driven = null;
        }
    }
}

