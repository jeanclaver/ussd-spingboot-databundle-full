package com.mtn.gn.databundleaccelerated.services;


import com.mtn.gn.databundleaccelerated.collections.MenuPrincipaleCollection;
import com.mtn.gn.databundleaccelerated.entities.MenuPrincipalEntity;

public interface MenuPrincipaleService {
    Iterable<MenuPrincipaleCollection> getMenu();
    MenuPrincipalEntity getMenuFromOracle();
    MenuPrincipalEntity saveMenuToOracle(MenuPrincipalEntity menuPrincipalEntity);
}
