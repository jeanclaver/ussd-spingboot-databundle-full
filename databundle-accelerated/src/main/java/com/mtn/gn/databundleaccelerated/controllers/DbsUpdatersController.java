package com.mtn.gn.databundleaccelerated.controllers;

import com.mtn.gn.databundleaccelerated.models.UpdateDatabasesModel;
import com.mtn.gn.databundleaccelerated.services.impl.UpdateServiceImpl;
import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.services.LogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/update")
public class DbsUpdatersController {
    @Autowired
    UpdateServiceImpl updateService;


    @PutMapping
    public UpdateDatabasesModel update() {
        LogServiceImpl.Log(
                this,
                LogLevel.INFO,
                ("****** update via the central updater ******\n").toUpperCase());
        return new UpdateDatabasesModel(
                this.updateService.updateRedis(),
                this.updateService.updateMongo(),
                this.updateService.updatePostgres(),
                this.updateService.updateOracle(),
                false
        );
    }
}
