package com.mtn.gn.databundleaccelerated;

import com.mtn.gn.Main;
import com.mtn.gn.databundleaccelerated.models.DatabundleTransactionModel;
import com.mtn.gn.databundleaccelerated.models.xml.param;
import com.mtn.gn.databundleaccelerated.services.impl.TransactionsServiceImpl;
import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.services.LogServiceImpl;
import kong.unirest.Unirest;
import org.apache.catalina.filters.ExpiresFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.UUID;

@SpringBootApplication
@EnableFeignClients
@RestController
public class DatabundleAcceleratedApplication{
    @Autowired
    TransactionsServiceImpl transactionsService;

    public static void main(String[] args) {
        if (System.getProperty("os.name").toLowerCase().contains("windows")) {
            Main.init("DATABUNDLE", "C:\\MTNLogs\\databundle");
        }
        else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            Main.init("DATABUNDLE", "/Users/macbook/logs/ussd_");
        }
        else  {
            Main.init("DATABUNDLE", "/app/");
        }

        SpringApplication.run(DatabundleAcceleratedApplication.class, args);
    }
}
