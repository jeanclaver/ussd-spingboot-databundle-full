package com.mtn.gn.databundleaccelerated.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "databundles_menu", schema = "app_user")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuPrincipalEntity {
    @Id
    private String name;
    @Column(length = 65535, columnDefinition = "text")
    private String value;
}
