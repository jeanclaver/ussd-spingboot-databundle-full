package com.mtn.gn.databundleaccelerated.repositories;

import com.mtn.gn.databundleaccelerated.collections.DataBundleMenuUpdater;
import org.springframework.data.repository.CrudRepository;

public interface UpdaterRepository extends CrudRepository<DataBundleMenuUpdater, String> {
    Iterable<DataBundleMenuUpdater> findAllByOrderByPosition();
}
