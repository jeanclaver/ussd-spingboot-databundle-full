package com.mtn.gn.databundleaccelerated.collections;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Document(collection = "databundle_transactions")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DatabundleTransactionCollection {
    @Id
    @Column(name = "transaction_id")
    private String transactionId;
    private String msisdn;
    @Column(name = "service_name")
    private String serviceName;
    private int amount;
    private String keyword;
    private String status;
    @Column(name = "created_at")
    private String createdAt;
}
