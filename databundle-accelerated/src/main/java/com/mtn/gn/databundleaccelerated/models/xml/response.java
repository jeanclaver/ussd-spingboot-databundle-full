package com.mtn.gn.databundleaccelerated.models.xml;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Data
@AllArgsConstructor
@NoArgsConstructor
public class response {
    private String msisdn = "";
    private String applicationResponse = null;
    private String appDrivenMenuCode= null;
    private FreeFlowResponseModel freeflow= new FreeFlowResponseModel();
}
