package com.mtn.gn.databundleaccelerated.repositories;

import com.mtn.gn.databundleaccelerated.entities.MenuPrincipalEntity;
import org.springframework.data.repository.CrudRepository;

public interface MenuPrincipalFromPostgresRepository extends CrudRepository<MenuPrincipalEntity, String> {
    MenuPrincipalEntity findByName(String name);
}
