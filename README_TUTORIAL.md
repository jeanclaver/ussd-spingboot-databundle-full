# **New DATABUNDLE** USSD APP MTN GC 

## Getting Started

To up and run this project, we have the following way

* Using Jenkins Automatique Pipelines
* Docker-Compose ou Docker-Swarm
* Make tool


### Prerequisites

* Having mongoDB, postgresDB up and running in the databundle-network overlay
* Exec all cmd directly on this node 10.4.0.247 which is the master of the cluster.
* **DOCKER**, **jdk-8**, **make** 

### Installing Prerequisites

* MONGODB: 

```$ sudo mkdir -p /data/db/databundle```

```$ sudo chmod -R 777 /data/db/databundle```

```$ docker run --name mongo-databundle-live -p 27080:27017   -e MONGO_INITDB_ROOT_USERNAME=app_user   -e MONGO_INITDB_ROOT_PASSWORD=APP_USER   -v /data/db/databundle:/data/db   --restart always   -d   mongo:4.2.1```

* POSTGRESDB: 


```$ sudo mkdir -p /data/postgres/databundle```

```$ sudo chmod -R 777 /data/postgres/databundle```

```$ docker run --name postgres-databundle-live  -e POSTGRES_USER=app_user  -e POSTGRES_PASSWORD=APP_USER  -p 5432:5432 -d  -v /data/postgres/databundle:/var/lib/postgresql/data  --restart always  postgres:11```

```$ ssh infraop@10.4.0.247 ```

```$ cd producution/ussd/databundle-full/ ```

To install up and run this application, you must have the rights to clone this [repos](git@10.4.0.119:mtn/ussd/databundle-full.git) 

```
$ git pull origin master 
```

## UP AND RUN WITH DOCKER

* ###### FIRST COMMAND IS TO INIT A SWARM CLUSTER AND CREATE AN OVERLAY NETWORK 

  + ###### CLUSTER INITIALISATION 
    ```bash
    $ docker swarm init
    ```
  
  + ###### NETWORK OVERLAY CREATION   
    ```bash
    $ docker network create --driver=overlay --attachable databundle-network
    ```

  * ###### RUN THE MONGO DB DATABASE
     ```bash
     $ docker-compose -f ./ecw/ecw-api-compose.yml up -d --build
     ```
    After the mongo db database is up and running and ready to accept connexion
    
* RUNNING THE APP WITH DOCKER COMPOSE
  * ###### EXEC THIS COMMAND BELLOW
       
     ```bash
     $ docker-compose up --build -d 
     ```

* RUNNING THE APP IN SWARM CLUSTER
     * ###### to run the app in the swarm, we have init the swarm first

     ```bash
     $ docker stack deploy -c docker-stack.ym momo   
     ```  

Here we go, you can test it on
```bash
$ http -a user:password http://10.4.0.24:9999/momo-service-api/payment/bundle/[MSISDN]/[AMOUNT]/[MOMO_SERVICE_ID]/[KEYWORD]/[NAME_OF_THE_APP]?narration=
```
httpi or curl 


## Built With

* [MongoDB](https://www.mongodb.com/) - The database for modern applications
* [Kotlin](https://kotlinlang.org/) - A modern programming language
                                      that makes developers happier.
                                      Open source forever 
* [Spring-Boot](https://spring.io/) - Spring makes programming Java quicker, easier, and safer for everybody. Spring’s focus on speed, simplicity, and productivity has made it the world's most popular Java framework.

## Developer 

[Mombe090](https://github.com/mombe090)

## Authors

* **Mamadou Yaya DIALLO** - *Initial work* - [Mamadou Yaya DIALLO](https://twitter.com/MamadouYaya)

## License

All right reserved


