all: build

databundle = "./databundle-accelerated"
momo-bonus-app = "./momo-accelerate"
mdp-account-info = "./mdp-accounts-infos"
oracle-db-interface = "./oracle-db-interface"
live-server = 'infraop@10.4.0.247'
uat-server = 'infraop@10.4.0.248'

build:  databundle-build momo-bonus-build mdp-account-build oracle-interface-build

databundle-build:
	 cd $(databundle) && make package

momo-bonus-build:
	  cd $(momo-bonus-app) && make package

mdp-account-build:
	cd $(mdp-account-info) && make package

oracle-interface-build:
	cd $(oracle-db-interface) && make package

push-img: databundle_img_push momo-bonus-app_img_push mdp_account_img_push oracle_interface_img_push

databundle_img_push:
	cd $(databundle) && make build-push-img

momo-bonus-app_img_push:
	cd $(momo-bonus-app) && make build-push-img

mdp_account_img_push:
	cd $(mdp-account-info) && make build-push-img

oracle_interface_img_push:
	cd $(oracle-db-interface) && make build-push-img

uat:
	docker-compose pull && docker-compose up -d --build

live:
	docker stack deploy --with-registry-auth -c docker-stack-full.yml DATABUNDLE
	#cd $(oracle-db-interface) && make golive
	#cd $(momo-bonus-app) && make golive
	#cd $(mdp-account-info) && make golive
	#cd $(databundle) && make golive
