package com.mtn.gn.mdpaccountsinfos.services;

import org.springframework.stereotype.Service;

@Service
public class Helper {
    public String byteConverter(double bytes) {
        long n = 1024;
        String s = "";
        double kb = bytes / n;
        double mb = kb / n;
        double gb = mb / n;
        double tb = gb / n;
        if(bytes < n) {
            s = bytes + " Bytes";
        } else if(bytes >= n && bytes < (n * n)) {
            s =  String.format("%.2f", kb) + " KB";
        } else if(bytes >= (n * n) && bytes < (n * n * n)) {
            s = String.format("%.2f", mb) + " MB";
        } else if(bytes >= (n * n * n) && bytes < (n * n * n * n)) {
            s = String.format("%.2f", gb) + " GB";
        } else if(bytes >= (n * n * n * n)) {
            s = String.format("%.2f", tb) + " TB";
        }
        return s;
    }
}
