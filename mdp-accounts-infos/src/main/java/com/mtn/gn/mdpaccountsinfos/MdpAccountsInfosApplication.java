package com.mtn.gn.mdpaccountsinfos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MdpAccountsInfosApplication {

    public static void main(String[] args) {
        SpringApplication.run(MdpAccountsInfosApplication.class, args);
    }

}
