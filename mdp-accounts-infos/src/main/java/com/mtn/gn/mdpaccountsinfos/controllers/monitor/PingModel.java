package com.mtn.gn.mdpaccountsinfos.controllers.monitor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PingModel {
    private String response;
}
