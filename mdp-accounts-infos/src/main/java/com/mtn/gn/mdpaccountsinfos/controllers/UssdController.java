package com.mtn.gn.mdpaccountsinfos.controllers;

import com.mtn.gn.mdpaccountsinfos.services.Helper;
import com.mtn.gn.mdpaccountsinfos.xml.FreeFlowResponseModel;
import com.mtn.gn.mdpaccountsinfos.xml.request;
import com.mtn.gn.mdpaccountsinfos.xml.response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pojos.MdpActiveBundleResponse;
import pojos.MdpAdvanceBundle;
import pojos.MdpPlanUsage;
import services.implementations.MdpServiceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/ussd")
public class UssdController {
    @Autowired
    Helper helper;
    @PostMapping(
            produces = {
                    "application/xml",
                    "text/xml"
            },
            consumes = {
                    "application/xml",
                    "text/xml"
            }
    )
    @ResponseBody
    response getCurrentBundles(@RequestBody request rq) {
        String driven = null;
        String freeFlow = "FC";

        String msg = null;

        MdpServiceImpl mdpService = new MdpServiceImpl();


        if (rq.getNewRequest() != null
                && rq.getMsisdn() != null
                && rq.getSubscriberInput() != null
                && rq.getSessionId() != null
        ) {
            try {
                if (rq.getNewRequest().equals("1")) {
                    freeFlow = "FC";
                    MdpActiveBundleResponse response = mdpService.currentBundles(rq.getMsisdn(), "1");

                    driven = null;
                    msg = this.getActiveBundle(response);
                }
                else {
                   driven = "775";
                   freeFlow = "FB";
                   msg = null;
                }


            } catch (IOException e) {
                e.printStackTrace();
                msg = "Nous rencontrons une erreur, veillez reessayez svp";
                driven = null;
            }
        }

        return new response(
                rq.getMsisdn(),
                msg,
                driven,
                new FreeFlowResponseModel(
                        freeFlow,
                        "N",
                        "0.0",
                        null
                )

        );
    }

    private String getActiveBundle(MdpActiveBundleResponse response ) {
        StringBuilder menu = new StringBuilder("MTN Pass Internet \n");
        if (response.getBundle().size() != 0) {
            List<MdpPlanUsage> activeBundles = new ArrayList<>();


            for (MdpPlanUsage t: response.getBundle()) {
                if (
                        !t.getPlanName().toLowerCase().contains("combo") &&
                                !t.getPlanName().toLowerCase().contains("comjaim") &&
                                !t.getPlanName().toLowerCase().contains("gratis") &&
                                !t.getPlanName().toLowerCase().contains("yellow") &&
                                !t.getPlanName().toLowerCase().contains("gratis") &&
                                !t.getPlanName().toLowerCase().contains("ayoba")
                ) {
                    activeBundles.add(t);
                }
            }

            int i = 1;
            if (activeBundles.size() > 0) {
                menu.append("Pass en cours \n");

                for (MdpPlanUsage bundle : activeBundles) {
                    String usage = this.helper.byteConverter(Double.parseDouble(bundle.getRamainingUsage()));
                    String trueUsage = "";
                    menu.append(i).append(". ").append("Pass de ")
                            .append(bundle.getPlanName())
                            .append(" solde=");
                    if (usage.contains("-1")) {
                        trueUsage = "illimite";
                        menu.append(trueUsage);
                    } else {
                        trueUsage = usage;
                        menu.append(trueUsage);
                    }
                        menu.append(" valide ");
                        menu.append(bundle.getValidityDate())
                            .append("\n");
                    i++;
                }
            } else {
                menu.append("\nVous n avez pas de pass en cours, tapez *100*3# pour acheter\n");
            }

            if (response.getAdvabceBundle().size() > 0) {
                List<MdpAdvanceBundle> advanceBundles = new ArrayList<>();

                menu.append("Forfait en attentes\n");



                for(MdpAdvanceBundle bundle : response.getAdvabceBundle()){
                    if (
                            !bundle.getAdvancePlanName().toLowerCase().contains("combo") &&
                                    !bundle.getAdvancePlanName().toLowerCase().contains("comjaim") &&
                                    !bundle.getAdvancePlanName().toLowerCase().contains("yellow bet") &&
                                    !bundle.getAdvancePlanName().toLowerCase().contains("gratis") &&
                                    !bundle.getAdvancePlanName().toLowerCase().contains("ayoba")
                    ) {
                        advanceBundles.add(bundle);
                    }
                }

                advanceBundles.forEach( bundle -> {
                    menu.append(bundle.getAdvancePlanCount()).append(" * ").append(bundle.getAdvancePlanName());
                });
            }
        }

        menu.append("\n# Retour");
       return menu.toString();
    }
}
