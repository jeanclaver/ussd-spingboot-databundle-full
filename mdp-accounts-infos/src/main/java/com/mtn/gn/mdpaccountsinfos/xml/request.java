package com.mtn.gn.mdpaccountsinfos.xml;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@Data
@AllArgsConstructor
@NoArgsConstructor
public class request {
    private String msisdn;
    private String subscriberInput;
    private String newRequest;
    private String sessionId;
    private parameters parameters = new parameters(
            new ArrayList<>()
    );
    private FreeFlowRequestModel freeflow= new FreeFlowRequestModel();
    private String transactionId;
}
