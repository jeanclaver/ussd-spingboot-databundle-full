package com.mtn.gn.momoaccelerate.enums;

public enum  MenuEnum {
    MENU_PRINCIPAL,
    MENU_24,
    MENU_48,
    MENU_WEEKLY,
    MENU_MONTHLY,
    MENU_INTER
}
