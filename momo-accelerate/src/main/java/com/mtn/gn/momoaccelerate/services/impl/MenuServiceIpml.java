package com.mtn.gn.momoaccelerate.services.impl;

import com.mtn.gn.momoaccelerate.collections.MenuCollection;
import com.mtn.gn.momoaccelerate.repositories.MenuRepository;
import com.mtn.gn.momoaccelerate.services.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuServiceIpml implements MenuService {
    @Autowired
    private MenuRepository menuRepository;


    @Override
    public Iterable<MenuCollection> getMenu() {
        return this.menuRepository.findAll();
    }

    @Override
    public MenuCollection getMenuBy(String type) {
        return this.menuRepository.findByBundleType(type);
    }
}
