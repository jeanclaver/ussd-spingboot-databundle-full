package com.mtn.gn.momoaccelerate.controllers;

import com.mtn.gn.momoaccelerate.collections.Bundle;
import com.mtn.gn.momoaccelerate.collections.MenuCollection;
import com.mtn.gn.momoaccelerate.enums.MenuEnum;
import com.mtn.gn.momoaccelerate.models.PaymentModel;
import com.mtn.gn.momoaccelerate.models.RedisCacheModel;
import com.mtn.gn.momoaccelerate.models.xml.FreeFlowResponseModel;
import com.mtn.gn.momoaccelerate.models.xml.parameters;
import com.mtn.gn.momoaccelerate.models.xml.request;
import com.mtn.gn.momoaccelerate.models.xml.response;
import com.mtn.gn.momoaccelerate.proxies.UnirestServiceProxy;
import com.mtn.gn.momoaccelerate.services.impl.HelperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class UssdController {
    private static final String MenuPrincipalHeader = "BONUS PASS MoMo\n\n";
    private static final String MenuDailyHeader = "BONUS PASS MoMo 24h\n\n";
    private static final String Menu48HHeader = "BONUS PASS MoMo 48h\n\n";
    private static final String MenuWeeklyHHeader = "BONUS PASS MoMo Semaine\n\n";
    private static final String MenuMonthlyHHeader = "BONUS PASS MoMo Mois\n\n";

    @Autowired
    HelperService helper;

    @Autowired
    UnirestServiceProxy proxy;


    @PostMapping(
            value = "/ussd",
            produces = {
                    "application/xml",
                    "text/xml"
            },
            consumes = {
                    "application/xml",
                    "text/xml"
            }
    )
    @ResponseBody
    public response ussd(@RequestBody request rq, HttpServletRequest req) {
        String freeFlow = "FC";
        RedisCacheModel cache = new RedisCacheModel();

        if (rq.getNewRequest() != null
                && rq.getMsisdn() != null
                && rq.getSubscriberInput() != null
                && rq.getSessionId() != null
        )
        {
            // FIRST REQUEST COMMING
            if (rq.getNewRequest().equals("1")) {
                 cache = new RedisCacheModel(
                        2,
                        rq.getSessionId(),
                        0,
                        MenuEnum.MENU_PRINCIPAL,
                        rq.getMsisdn(),
                        null,
                        "FC",
                        "",
                        null

                );

                cache.msg = this.goPrincipaleMenu(cache);
                cache.driven = null;
                this.helper.saveCache(cache);
            }
            else {
                //Getting information save in cache
                 cache = this.helper.getCache(rq.getSessionId());
                //Differents steps handler
                stepHandler(rq, cache);

            }
        }

        /*if (rq.getNewRequest().equals("0") &&
                rq.getSubscriberInput().equals("#") &&
                cache.msg
        )*/

        response response = new response(
                rq.getMsisdn(),
                cache.msg,
                cache.driven,
                new FreeFlowResponseModel(
                        cache.freeFlow,
                        "N",
                        "0.0",
                        null
                )

        );
        System.out.println(response);
        return response;

    }

    private void stepHandler(@RequestBody request rq, RedisCacheModel cache) {

        if (cache == null) {
            cache = new RedisCacheModel(
                    2,
                    rq.getSessionId(),
                    0,
                    MenuEnum.MENU_PRINCIPAL,
                    rq.getMsisdn(),
                    null,
                    "FC",
                    "",
                    null

            );
            cache.choice = 0;
            cache.step = 2;
            cache.menu = MenuEnum.MENU_PRINCIPAL;
            cache.msg = goPrincipaleMenu(cache);
            this.helper.saveCache(cache);
        }
        else  if (cache.step == 2) {
            if (!rq.getSubscriberInput().trim().equals("#")) {
                try {
                    cache.choice = Integer.parseInt(rq.getSubscriberInput().trim());

                    // PRINCIPAL MENU CHOICES
                    getPrincipalMenuChoice(cache);
                }
                catch (Exception e) {
                    StringBuilder menu = new StringBuilder().append(MenuPrincipalHeader);
                    menu.append("Veillez saisir un entier valide.");

                    e.printStackTrace();

                    cache.driven = null;
                    cache.msg = menu.toString() ;
                    cache.freeFlow = "FB";
                }
            }
            //User input is for back
            else {
                cache.msg = null;
                cache.driven = "775";
                cache.freeFlow = "FB";
                cache.setStep(1);
                cache.setMenu(MenuEnum.MENU_PRINCIPAL);
                this.helper.saveCache(cache);
            }
        }
        else if (cache.step == 3) {
            if (rq.getSubscriberInput().trim().equals("#")) {
                cache.msg = goPrincipaleMenu(cache);
                cache.setStep(2);
                this.helper.saveCache(cache);
                cache .setMenu(MenuEnum.MENU_PRINCIPAL);

            }
            else {
                // Getting User selection
                Bundle selectBundle = getMenuSelectedByUser(rq, cache);
                // Checking on good choice have been done
                if (selectBundle != null) {
                    PaymentModel rep = this.proxy.sendRequestToEcw(rq.getMsisdn(), selectBundle.getPrice(), selectBundle.getKeyword());

                    if (rep.getStatusCode().equals("1000")) {
                        cache.msg = "Veuillez patienter, vous recevrez le message de confirmation dans un instant.";
                    }
                    else if (rep.getStatusCode().equals("529")) {
                        cache.msg = "Cher client, vous n avez pas suffisamment d argent dans votre compte. Merci de recharger et reessayer.";
                    }
                    else {
                        cache.msg = "Une erreur est survenue, veillez reessayer plus tard! MTN vous remercie pour votre fidelite";
                    }
                    this.helper.deleteCache(rq.getSessionId());
                    cache.freeFlow = "FB";
                    cache.driven = null;
                } else {
                    cache.msg = "Une erreur est survenue, veillez reessayer plus tard! MTN vous remercie pour votre fidelite";
                    cache.freeFlow = "FB";
                    cache.driven = null;
                }
            }
        }
    }

    private void getPrincipalMenuChoice(RedisCacheModel cache) {
        switch (cache.choice) {
            // Daily Bundle
            case 1:
                cache.msg = goDailyMenu(cache);
                cache.setStep(3);
                cache .setMenu(MenuEnum.MENU_24);
                break;

            // 48h bundles
            case 2:
                cache.msg = go48hMenu(cache);
                cache.setStep(3);
                cache.setMenu(MenuEnum.MENU_48);
                break;

            // Weekly bundles
            case 3:
                cache.msg = goWeeklyMenu(cache);
                cache.setStep(3);
                cache .setMenu(MenuEnum.MENU_WEEKLY);
                break;

            // Monthly bundles
            case 4:
                cache.msg = goMonthlyMenu(cache);
                cache.setStep(3);
                cache .setMenu(MenuEnum.MENU_MONTHLY);
                break;

            default: cache.msg = goPrincipaleMenu(cache);
                cache .setMenu(MenuEnum.MENU_PRINCIPAL);
        }
        this.helper.saveCache(cache);
    }

    private Bundle getMenuSelectedByUser(@RequestBody request rq, RedisCacheModel cache) {
        if (cache.getMenu().equals(MenuEnum.MENU_24)) {
            MenuCollection resp = this.helper.getDailtyMenu();
            for (Bundle b : resp.getBundles()) {
                if (b.getPosition().replace(".0", "").equals(rq.getSubscriberInput().trim())) {
                    return b;
                }
            }
        }
        else if (cache.getMenu().equals(MenuEnum.MENU_48)) {
            MenuCollection resp = this.helper.get48hMenu();
            for (Bundle b : resp.getBundles()) {
                if (b.getPosition().replace(".0", "").equals(rq.getSubscriberInput().trim())) {
                    return b;
                }
            }
        }
        else if (cache.getMenu().equals(MenuEnum.MENU_WEEKLY)) {
            MenuCollection resp = this.helper.getWeeklyMenu();
            for (Bundle b : resp.getBundles()) {
                if (b.getPosition().replace(".0", "").equals(rq.getSubscriberInput().trim())) {
                    return b;
                }
            }
        }
        else if (cache.getMenu().equals(MenuEnum.MENU_MONTHLY)) {
            MenuCollection resp = this.helper.getMonthlyMenu();
            for (Bundle b : resp.getBundles()) {
                if (b.getPosition().replace(".0", "").equals(rq.getSubscriberInput().trim())) {
                    return b;
                }
            }
        }
        return null;
    }


    private String goPrincipaleMenu(RedisCacheModel cache) {
        StringBuilder menu = new StringBuilder();
        menu.append(MenuPrincipalHeader);

        List<MenuCollection> menuPrincipal = (List<MenuCollection>) this.helper.getThePrincipalMenu();
        menuPrincipal.forEach(
                t -> {
                    menu.append(t.getPosition().replace(".0", "")).append(". ").append(t.getName()).append("\n");
                }
        );
        cache.driven = null;
        cache.freeFlow = "FC";
        menu.append("# Retour");
        return menu.toString();
    }

    private String goDailyMenu(RedisCacheModel cache) {
        StringBuilder menu = new StringBuilder();
        menu.append(MenuDailyHeader);

        MenuCollection menuDaily = this.helper.getDailtyMenu();
        if (menuDaily != null) {
            menuDaily.getBundles().forEach(
                    t -> {
                        menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                    }
            );
            cache.driven = null;
            cache.freeFlow = "FC";
            menu.append("# Retour");
            return menu.toString();
        }
        cache.freeFlow = "FB";
        return "Erreur serveur, veillez reesayez plutard \n";
    }

    private String go48hMenu(RedisCacheModel cache) {
        StringBuilder menu = new StringBuilder();
        menu.append(Menu48HHeader);

        MenuCollection menu48H = this.helper.get48hMenu();

        if (menu48H != null) {
            menu48H.getBundles().forEach(
                    t -> {
                        menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                    }
            );

            cache.driven = null;
            menu.append("# Retour");
            return menu.toString();
        }
        cache.freeFlow = "FB";
        return "Erreur serveur, veillez reesayez plutard \n";
    }

    private String goWeeklyMenu(RedisCacheModel cache) {
        StringBuilder menu = new StringBuilder();
        menu.append(MenuWeeklyHHeader);

        MenuCollection menuWeekly = this.helper.getWeeklyMenu();

        if (menuWeekly != null) {
            menuWeekly.getBundles().forEach(
                    t -> {
                        menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                    }
            );

            cache.driven = null;
            cache.freeFlow = "FC";
            menu.append("# Retour");
            return menu.toString();
        }
        cache.freeFlow = "FB";
        return "Erreur serveur, veillez reesayez plutard \n";
    }

    private String goMonthlyMenu(RedisCacheModel cache) {
        StringBuilder menu = new StringBuilder();
        menu.append(MenuMonthlyHHeader);

        MenuCollection menuMonthly = this.helper.getMonthlyMenu();

        if (menuMonthly != null) {
            menuMonthly.getBundles().forEach(
                    t -> {
                        menu.append(t.getPosition()).append(". ").append(t.getName()).append("\n");
                    }
            );

            cache.driven = null;
            cache.freeFlow = "FC";
            menu.append("# Retour");
            return menu.toString();
        }
        cache.freeFlow = "FB";
        return "Erreur serveur, veillez reesayez plutard \n";
    }
}

