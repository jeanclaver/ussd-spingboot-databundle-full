package com.mtn.gn.momoaccelerate.collections;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "momo_bonus_bundles_updater")
public class MenuUpdaterCollection {
    private String _id;
    private String position;
    private String bundleType;
    private String name;
    private List<Bundle> bundles;
}
