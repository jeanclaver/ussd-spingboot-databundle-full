package com.mtn.gn.momoaccelerate.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("spring.profiles")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RunningEnvConfigRessources {
    private String active;
}
