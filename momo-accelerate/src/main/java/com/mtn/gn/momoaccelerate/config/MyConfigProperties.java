package com.mtn.gn.momoaccelerate.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("my.properties")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyConfigProperties {
    private String oracleInterfacer;
    private String shortCode;
    private String ecwGatewayIp;
    private String ecwGatewayPort;
    private String ecwGatewayEnv;

}
