package com.mtn.gn.momoaccelerate.repositories;

import com.mtn.gn.momoaccelerate.collections.MenuUpdaterCollection;
import org.springframework.data.repository.CrudRepository;

public interface MenuUpdateRepository extends CrudRepository<MenuUpdaterCollection, String> {
}
