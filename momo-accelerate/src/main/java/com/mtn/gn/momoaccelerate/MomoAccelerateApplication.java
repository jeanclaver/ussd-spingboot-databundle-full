package com.mtn.gn.momoaccelerate;

import com.mtn.gn.Main;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MomoAccelerateApplication {
    public static void main(String[] args) {
        if (System.getProperty("os.name").toLowerCase().contains("windows")) {
            Main.init("MOMO_BONUS_APP", "C:\\MTNLogs\\databundle");
        }
        else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            Main.init("MOMO_BONUS_APP", "/Users/macbook/logs/ussd_");
        }
        else  {
            Main.init("MOMO_BONUS_APP", "/app/");
        }
        SpringApplication.run(MomoAccelerateApplication.class, args);
    }
}
