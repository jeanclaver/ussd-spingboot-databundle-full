package com.mtn.gn.momoaccelerate.seeds;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mtn.gn.enums.LogLevel;
import com.mtn.gn.momoaccelerate.collections.MenuCollection;
import com.mtn.gn.momoaccelerate.collections.MenuUpdaterCollection;
import com.mtn.gn.momoaccelerate.config.RunningEnvConfigRessources;
import com.mtn.gn.momoaccelerate.repositories.MenuRepository;
import com.mtn.gn.momoaccelerate.repositories.MenuUpdateRepository;
import com.mtn.gn.momoaccelerate.repositories.RedisCacheRepository;
import com.mtn.gn.services.LogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

@Service
@Order(1)
public class MenuRedisCacheSeeder implements CommandLineRunner {
    @Autowired
    RedisCacheRepository redisCacheRepository;

    @Autowired
    MenuUpdateRepository menuUpdateRepository;

    @Autowired
    MenuRepository menuRepository;

    @Autowired
    RunningEnvConfigRessources runningEnvConfigRessources;

    @Override
    public void run(String... args) throws Exception {

        List<MenuUpdaterCollection> updateMenu = (List<MenuUpdaterCollection>) this.menuUpdateRepository.findAll();

        if (updateMenu.size() == 0) {
            try {
                File file;

                if (runningEnvConfigRessources.getActive().equals("dev")) {
                    file   = new ClassPathResource("data/menu.json").getFile();
                } else {
                    file = new File("/app/menu.json");
                }

                String text = new String(Files.readAllBytes(file.toPath()));
                try {
                    // SAVING DATA TO LIVE COLLECTION
                    ObjectMapper objectMapper = new ObjectMapper();

                    // MENU GOT FROM REDIS CACHE
                    List<MenuUpdaterCollection> menu = objectMapper.readValue(
                            text
                            ,
                            new TypeReference<List<MenuUpdaterCollection>>(){}
                    );

                    this.menuUpdateRepository.deleteAll();
                    this.menuUpdateRepository.saveAll(menu);
                    // Mise a jour du updater par le fichier

                    this.menuRepository.deleteAll();
                    menu.forEach(
                            f -> {
                                this.menuRepository.save(
                                        new MenuCollection(
                                                null,
                                                f.getPosition(),
                                                f.getBundleType(),
                                                f.getName(),
                                                f.getBundles()
                                        ));
                            });

                    LogServiceImpl.Log(
                            this,
                            LogLevel.INFO,
                            ("****** MENU PRINCIPAL ADDED TO CACHE FROM FILE ******").toUpperCase());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            LogServiceImpl.Log(
                    this,
                    LogLevel.INFO,
                    ("****** APPLICATION START WITH ADDING MENU FROM MONGO UPDATER COLLECTION ******").toUpperCase());

            this.menuRepository.deleteAll();
            updateMenu.forEach(
                    f -> {
                        this.menuRepository.save(
                                new MenuCollection(
                                        null,
                                        f.getPosition(),
                                        f.getBundleType(),
                                        f.getName(),
                                        f.getBundles()
                                ));
                    });

            LogServiceImpl.Log(
                    this,
                    LogLevel.INFO,
                    ("****** MENU PRINCIPAL ADDED  FROM MONGO UPDATER COLLECTION ******").toUpperCase());
        }

    }
}
