package com.mtn.gn.momoaccelerate.services;

import com.mtn.gn.momoaccelerate.collections.MenuCollection;

public interface MenuService {
    Iterable<MenuCollection> getMenu();
    MenuCollection getMenuBy(String name);
}
