package com.mtn.gn.momoaccelerate.collections;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bundle {
    private String _id;
    private String position;
    private String name;
    private int price;
    private String keyword;
    private String si;
}
