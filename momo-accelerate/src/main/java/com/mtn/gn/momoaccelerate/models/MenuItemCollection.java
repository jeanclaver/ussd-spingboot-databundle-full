package com.mtn.gn.momoaccelerate.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuItemCollection {
    private int position;
    private String name;
    private int price;
    private String keyword;
    private String si;
}
