package com.mtn.gn.momoaccelerate.models.xml;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FreeFlowRequestModel {
    private String mode;
}
