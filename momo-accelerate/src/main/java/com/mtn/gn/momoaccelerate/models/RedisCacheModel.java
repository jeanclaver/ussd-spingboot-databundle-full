package com.mtn.gn.momoaccelerate.models;

import com.mtn.gn.momoaccelerate.collections.Bundle;
import com.mtn.gn.momoaccelerate.enums.MenuEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RedisCacheModel implements Serializable {
    public int step;
    public String ussdSession;
    public int choice;
    public MenuEnum menu;
    public String msisdn;
    public Bundle selectedBundle;
    public String freeFlow;
    public String msg;
    public String driven;

}
