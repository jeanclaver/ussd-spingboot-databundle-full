package com.mtn.gn.momoaccelerate.services.impl;

import com.mtn.gn.momoaccelerate.collections.MenuCollection;
import com.mtn.gn.momoaccelerate.models.PaymentModel;
import com.mtn.gn.momoaccelerate.models.RedisCacheModel;
import com.mtn.gn.momoaccelerate.repositories.RedisCacheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelperService {
    @Autowired
    private MenuServiceIpml menuServiceIpml;


    @Autowired
    private RedisCacheRepository cacheRepo;

    public Iterable<MenuCollection> getThePrincipalMenu(){
        return this.menuServiceIpml.getMenu();
    }

    public MenuCollection getDailtyMenu(){
        return this.menuServiceIpml.getMenuBy("DIALY");
    }

    public MenuCollection get48hMenu(){
        return this.menuServiceIpml.getMenuBy("48H");
    }

    public MenuCollection getWeeklyMenu(){
        return this.menuServiceIpml.getMenuBy("WEEKLY");
    }


    public MenuCollection getMonthlyMenu(){
        return this.menuServiceIpml.getMenuBy("MONTHLY");
    }

    public RedisCacheModel saveCache (RedisCacheModel cache){
        this.cacheRepo.addItem(cache);

        return this.cacheRepo.getItem(cache.ussdSession);
    }

    public RedisCacheModel getCache (String id){
        return this.cacheRepo.getItem(id);
    }
    public void deleteCache (String id){
         this.cacheRepo.deleteItem(id);
    }

}
