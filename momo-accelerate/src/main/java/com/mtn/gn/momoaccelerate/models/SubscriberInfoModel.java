package com.mtn.gn.momoaccelerate.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubscriberInfoModel{
    private String msisdn;
    private int amount;
    private String serviceDsc;
}
