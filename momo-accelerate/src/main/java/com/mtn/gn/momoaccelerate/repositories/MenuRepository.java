package com.mtn.gn.momoaccelerate.repositories;

import com.mtn.gn.momoaccelerate.collections.MenuCollection;
import org.springframework.data.repository.CrudRepository;

public interface MenuRepository extends CrudRepository<MenuCollection, String> {
    MenuCollection findByBundleType(String type);
}
