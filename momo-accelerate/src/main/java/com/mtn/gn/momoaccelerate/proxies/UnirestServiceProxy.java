package com.mtn.gn.momoaccelerate.proxies;

import com.mtn.gn.momoaccelerate.config.MyConfigProperties;
import com.mtn.gn.momoaccelerate.models.PaymentModel;
import kong.unirest.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnirestServiceProxy {
    @Autowired
    MyConfigProperties myConfigProperties;

    public PaymentModel sendRequestToEcw(String msisdn, int amount, String keyword) {
        boolean isLive = false;
        String sp = "momo100.sp";

        if (this.myConfigProperties.getEcwGatewayEnv().equals("LIVE")) {
            isLive = true;
        }

        if (!this.myConfigProperties.getEcwGatewayIp().trim().endsWith("24")) {
            sp = "momo100c.sp";
        }

        String url = "http://"+ this.myConfigProperties.getEcwGatewayIp() + ":" + this.myConfigProperties.getEcwGatewayPort() +"/ecw/api/v2/payment/"+ isLive +"/bundle/" +
                msisdn +
                "/" + amount +
                "/"+ sp +"/" +
                keyword +
                "/DATABUNDLE?message=DATABUNDLE_APP&narration=Forfait Internet";

        return Unirest.get(url)
                .basicAuth("mtn-momo", "Qwpatp2019@##*.")
                .header("Content-Type", "application/json")
                .socketTimeout(10000)
                .connectTimeout(10000)
                .asObject(PaymentModel.class)
                .getBody();
    }
}
