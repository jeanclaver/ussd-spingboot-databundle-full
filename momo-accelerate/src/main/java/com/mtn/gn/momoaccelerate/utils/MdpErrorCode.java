package com.mtn.gn.momoaccelerate.utils;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class  MdpErrorCode {
    private String errorMsg;


    public void handleErrorCode(int code) {
        switch (code) {
            case 501: this.errorMsg = "Le keyword est absent\n";
                break;

            case 502: this.errorMsg = "L ancien plan est manquant\n";
                break;

            case 503: this.errorMsg = "Impossible de souscrire un pass internet, veillez contacter le service client au 111\n";
                break;

            case 504:
            case 505:
                this.errorMsg = "Vous n est ete pas autorise a souscrire ac service \n";
                break;

            case 506: this.errorMsg = "Invalid old plan \n";
                break;

            case 516: this.errorMsg = "Cher client, vous n avez pas suffisamment d argent dans votre compte. Merci de recharger et reessayer.\n";
                break;
            case 517:
                this.errorMsg = "Une erreur est survenue, veillez reessayer plus tard! MTN vous remercie pour votre fidelite\n";
            default: this.errorMsg = "Une erreur est survenue, veillez reessayer plus tard! MTN vous remercie pour votre fidelite\n";
        }

    }
}
