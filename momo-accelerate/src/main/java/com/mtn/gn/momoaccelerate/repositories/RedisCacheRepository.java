package com.mtn.gn.momoaccelerate.repositories;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mtn.gn.momoaccelerate.models.RedisCacheModel;
import com.mtn.gn.momoaccelerate.utils.Minify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Repository
public class RedisCacheRepository {
    @Autowired
    RedisTemplate redisTemplate;

    public static String KEY = "menu";

    /*Getting all Items from tSable*/
    public Map<String, RedisCacheModel> getAllItems(){
        return redisTemplate.opsForHash().entries(KEY);
    }

    /*Getting a specific item by item id from table*/
    public RedisCacheModel getItem(String itemId){

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String response = (String) redisTemplate.opsForHash().get(itemId + "_" + KEY,itemId);
            if (response != null) {
                return objectMapper.readValue(
                        response,
                        RedisCacheModel.class
                );
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getMenuPrincipal(){
        return (String) redisTemplate.opsForHash().get("MENU","MENU_PRINCIPAL");
    }

    /*Adding an item into redis database*/
    public void addItem(RedisCacheModel cache){
        String key = cache.getUssdSession() + "_" + KEY;
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            String cacheJson = ow.writeValueAsString(cache);
            cacheJson = new Minify().minify(cacheJson);
            redisTemplate.opsForHash().put(key, cache.getUssdSession(), cacheJson);
            redisTemplate.expireAt(key, new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public void addMenuPrincipal(String menu){
        redisTemplate.opsForHash().put("MENU", "MENU_PRINCIPAL", menu);
    }
    /*delete an item from database*/
    public void deleteItem(String id){
        redisTemplate.opsForHash().delete(KEY,id);
    }

    /*update an item from database*/
    public void updateItem(RedisCacheModel cache){
        addItem(cache);
    }
}
