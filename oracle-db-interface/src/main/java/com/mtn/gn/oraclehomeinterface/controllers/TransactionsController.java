package com.mtn.gn.oraclehomeinterface.controllers;

import com.mtn.gn.oraclehomeinterface.entities.DatabundleTransactionEntity;
import com.mtn.gn.oraclehomeinterface.services.impl.DatabundleTransactionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transactions")
public class TransactionsController {
    @Autowired
    private DatabundleTransactionServiceImpl databundleTransactionService;

    @PostMapping("databundle")
    public ResponseEntity<DatabundleTransactionEntity> addTransaction(
            @RequestBody DatabundleTransactionEntity data
    ) {
       return new ResponseEntity<>(this.databundleTransactionService.saveTransaction(data), HttpStatus.CREATED);
    }
}
