package com.mtn.gn.oraclehomeinterface.services;


import com.mtn.gn.oraclehomeinterface.entities.MenuPrincipalEntity;

public interface MenuPrincipaleService {
    MenuPrincipalEntity getMenu();
    MenuPrincipalEntity saveMenuToOracle(MenuPrincipalEntity menuPrincipalEntity);
}
