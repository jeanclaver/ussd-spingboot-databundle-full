package com.mtn.gn.oraclehomeinterface.controllers;

import com.mtn.gn.oraclehomeinterface.entities.MenuPrincipalEntity;
import com.mtn.gn.oraclehomeinterface.services.impl.MenuPrincipalServiceIpml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private MenuPrincipalServiceIpml menuPrincipalServiceIpml;

    @PostMapping("databundle/save")
    public ResponseEntity<MenuPrincipalEntity> saveMenu(
            @RequestBody MenuPrincipalEntity data
    ) {
        return new ResponseEntity<>(this.menuPrincipalServiceIpml.saveMenuToOracle(data), HttpStatus.CREATED);
    }
}
