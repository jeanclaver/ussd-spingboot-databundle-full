package com.mtn.gn.oraclehomeinterface.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "new_databundles_menu", schema = "app_user")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuPrincipalEntity {
    @Id
    private String name;
    private String value;
}
