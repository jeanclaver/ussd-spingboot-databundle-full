package com.mtn.gn.oraclehomeinterface;

import com.mtn.gn.Main;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OracleHomeInterfaceApplication {

    public static void main(String[] args)
    {
        if (System.getProperty("os.name").toLowerCase().contains("windows")) {
            Main.init("ORACLE_DB_INTERFACER", "C:\\MTNLogs\\databundle");
        }
        else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            Main.init("ORACLE_DB_INTERFACER", "/Users/macbook/logs/ussd_");
        }
        else  {
            Main.init("ORACLE_DB_INTERFACER", "/app/");
        }
        SpringApplication.run(OracleHomeInterfaceApplication.class, args);
    }

}
