package com.mtn.gn.oraclehomeinterface.repositories;

import com.mtn.gn.oraclehomeinterface.entities.MenuPrincipalEntity;
import org.springframework.data.repository.CrudRepository;

public interface MenuPrincipalRepository extends CrudRepository<MenuPrincipalEntity, String> {
    MenuPrincipalEntity findByName(String name);
}
