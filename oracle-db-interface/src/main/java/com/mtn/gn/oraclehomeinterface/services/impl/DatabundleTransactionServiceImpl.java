package com.mtn.gn.oraclehomeinterface.services.impl;

import com.mtn.gn.oraclehomeinterface.entities.DatabundleTransactionEntity;
import com.mtn.gn.oraclehomeinterface.repositories.DatabundleTransactionRepository;
import com.mtn.gn.oraclehomeinterface.services.DatabundleTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DatabundleTransactionServiceImpl implements DatabundleTransactionService {
    @Autowired
    private DatabundleTransactionRepository databundleTransactionRepository;
    @Override
    public DatabundleTransactionEntity saveTransaction(DatabundleTransactionEntity databundleTransactionEntity) {
        return this.databundleTransactionRepository.save(databundleTransactionEntity);
    }
}
