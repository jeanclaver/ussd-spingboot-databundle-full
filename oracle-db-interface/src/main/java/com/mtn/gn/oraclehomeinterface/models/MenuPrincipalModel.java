package com.mtn.gn.oraclehomeinterface.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuPrincipalModel {
    private String name;
    private String value;
}
