package com.mtn.gn.oraclehomeinterface.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "new_databundle_transactions", schema = "app_user")
public class DatabundleTransactionEntity {
    @Id
    @Column(name = "transaction_id")
    private String transactionId;
    private String msisdn;
    @Column(name = "service_name")
    private String serviceName;
    private int amount;
    private String keyword;
    private String status;
    @Column(name = "created_at")
    private String createdAt;
}
