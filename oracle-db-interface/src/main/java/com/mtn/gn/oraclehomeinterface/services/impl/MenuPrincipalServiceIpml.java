package com.mtn.gn.oraclehomeinterface.services.impl;

import com.mtn.gn.oraclehomeinterface.entities.MenuPrincipalEntity;
import com.mtn.gn.oraclehomeinterface.repositories.MenuPrincipalRepository;
import com.mtn.gn.oraclehomeinterface.services.MenuPrincipaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuPrincipalServiceIpml implements MenuPrincipaleService {

    @Autowired
    private MenuPrincipalRepository repositoryOracle;

    @Override
    public MenuPrincipalEntity getMenu() {
        return this.repositoryOracle.findByName("MENU");
    }

    @Override
    public MenuPrincipalEntity saveMenuToOracle(MenuPrincipalEntity menuPrincipalEntity) {
        return this.repositoryOracle.save(menuPrincipalEntity);
    }
}
