package com.mtn.gn.oraclehomeinterface.repositories;

import com.mtn.gn.oraclehomeinterface.entities.DatabundleTransactionEntity;
import org.springframework.data.repository.CrudRepository;

public interface DatabundleTransactionRepository extends CrudRepository<DatabundleTransactionEntity, String> {
}
